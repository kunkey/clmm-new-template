<?php
class Proxy_Bridge
{
    public $token;
    public $type;
    public $amount;
    public $tradetype;

    public function _sendData()
    {
        try
        {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://vista.trade/api/wallet/binaryoption/bet',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{"betType":"' . $this->type . '","betAmount":' . (double)$this->amount . ',"betAccountType":"' . $this->tradetype . '"}',
                CURLOPT_HTTPHEADER => array(
                    'authority: vista.trade',
                    'sec-ch-ua: "Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
                    'accept: application/json, text/plain, */*',
                    'content-type: application/json;charset=UTF-8',
                    'authorization: Bearer ' . $this->token,
                    'sec-ch-ua-mobile: ?0',
                    'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
                    'sec-ch-ua-platform: "Linux"',
                    'origin: https://vista.trade',
                    'sec-fetch-site: same-origin',
                    'sec-fetch-mode: cors',
                    'sec-fetch-dest: empty',
                    'referer: https://vista.trade/index',
                    'accept-language: vi,vi-VN;q=0.9,en-US;q=0.8,en;q=0.7',
                    'cookie: WFCOUNTRY=vi'
                ) ,
            ));

            $response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            if($httpcode == 401) {
                echo json_encode(array(
                    'status' => false,
                    'message' => 'Invalid Token',
                    'code' => 401
                ));
            }else if($httpcode == 429) {
                echo json_encode(array(
                    'status' => false,
                    'message' => 'Server Blocked',
                    'code' => 429
                ));
            }else if($httpcode == 500) {
                echo json_encode(array(
                    'status' => false,
                    'message' => 'Error Handle',
                    'code' => 500
                ));
            }else if($httpcode == 200) {
                $json = $response;
                echo json_encode(array(
                    'status' => true,
                    'message' => 'Server Actived',
                    'code' => 200,
                    'data' => json_decode($json)
                ));
            }
        }
        catch(Exception $e)
        {
            echo json_encode(array(
                'status' => false,
                'message' => $e->getMessage(),
                'code' => 500,
            ));
        }
    }
}

$init = new Proxy_Bridge;
$init->token = $_GET["token"];
$init->type = $_GET["type"];
$init->amount = $_GET["amount"];
$init->tradetype = $_GET["tradetype"];
$init->_sendData();