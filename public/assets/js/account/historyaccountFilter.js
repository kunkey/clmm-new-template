const filter = () => {
    const startAt = $('input[name="datepickerStart"]').val().trim();
    const endAt = $('input[name="datepickerEnd"]').val().trim();

    $.ajax({
        "url": localStorage.getItem('api') + `/api/user/account/static?fromDate=${startAt}&toDate=${endAt}`,
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {
        $("#profits").html(`${NumFloor(resp.data.profits)}`);
        $("#wincount").html(`${resp.data.winCount}`);
        $("#losecount").html(`${resp.data.loseCount}`);
    }).fail((resp) => {
        //killSession();
    });
}