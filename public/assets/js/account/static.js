var access_token = localStorage.getItem('access_token');

$(document).ready(() => {
    $.ajax({
        "url": localStorage.getItem('api') + "/api/user/static",
        "method": "GET",
        "dataType": "json",
        "headers": {
          "Access-Control-Allow-Origin": "*",
          "Content-type": "application/json",
          "Authorization": `Bearer ${access_token}` 
        },
      }).done((resp) => {
          $("#wincount").html(`${resp.data.winCount}`);
          $("#losecount").html(`${resp.data.loseCount}`);
          $("#winmoney").html(`$${resp.data.winMoney}`);
          $("#losemoney").html(`$${resp.data.loseMoney}`);
      }).fail((resp) => {
        killSession();
      });
});