var access_token = localStorage.getItem('access_token');

$(document).ready(() => {
    $.ajax({
        "url": localStorage.getItem('api') + "/api/user/account/get-account-added",
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {
        if (resp.status) {
            $("#emailaddaccount").val(resp.account);
            $('#emailaddaccount').prop('readonly', true);
        }
    }).fail((resp) => {
        killSession();
    });
});