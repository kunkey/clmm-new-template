$(document).ready(() => {
    $("#analyticContainer").hide();

    $.ajax({
        "url": localStorage.getItem('api') + `/api/user/account/setting/${localStorage.getItem('ID_QUERY')}`,
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {
        if (resp.status) {

            if (resp.data.betLogic !== null) {
                let logicChoicingText;
                for (var logicList of resp.data.betLogicList) {
                    if (resp.data.betLogic == logicList.value) {
                        logicChoicingText = logicList.text;
                    }
                }
                const optionDefault = `<option value="${resp.data.betLogic}">${logicChoicingText}</option>`;
                $("#betLogic").append(optionDefault);
            }

            for (var logicList of resp.data.betLogicList) {
                if (resp.data.betLogic != logicList.value) {
                    const options = `<option value="${logicList.value}">${logicList.text}</option>`;
                    $("#betLogic").append(options);
                }
            }

            if (resp.data.balanceType == null) {
                $("#betType").append(`<option class="form-control" value="DEMO">Tài Khoản Demo</option>`);
                $("#betType").append(`<option class="form-control" value="LIVE">Tài Khoản Thật</option>`);
            } else if (resp.data.balanceType == "LIVE") {
                $("#betType").append(`<option class="form-control" value="LIVE">Tài Khoản Thật</option>`);
                $("#betType").append(`<option class="form-control" value="DEMO">Tài Khoản Demo</option>`);
            } else if (resp.data.balanceType == "DEMO") {
                $("#betType").append(`<option class="form-control" value="DEMO">Tài Khoản Demo</option>`);
                $("#betType").append(`<option class="form-control" value="LIVE">Tài Khoản Thật</option>`);
            }

            if (resp.data.betType == null) {
                $("#betWith").append(`<option class="form-control" value="percent">Bet Theo % Của Tài Khoản</option>`);
                $("#betWith").append(`<option class="form-control" value="money">Đặt Tiền Mỗi Lệnh</option>`);
            } else if (resp.data.betType == "money") {
                $("#betWith").append(`<option class="form-control" value="money">Đặt Tiền Mỗi Lệnh</option>`);
                $("#betWith").append(`<option class="form-control" value="percent">Bet Theo % Của Tài Khoản</option>`);
            } else if (resp.data.betType == "percent") {
                $("#betWith").append(`<option class="form-control" value="percent">Bet Theo % Của Tài Khoản</option>`);
                $("#betWith").append(`<option class="form-control" value="money">Đặt Tiền Mỗi Lệnh</option>`);
            }


            $(".stop-loss").html(String(resp.data.betLoseMax).replace("-", ""));
            $(".take-profit").html(resp.data.betWinMax);
            $("#bot_stop_loss").val(String(resp.data.betLoseMax).replace("-", ""));
            $("#bot_take_profit").val(resp.data.betWinMax);

            $("#betMoney").val(resp.data.betMoney);
            $("#betStatus").prop("checked", true);
            $("#profitsDay").html(`${NumFloor(resp.data.profitsToday)}`);
            $("#profitsAll").html(`${NumFloor(resp.data.profitsAll)}`);

            $("#betSetMoney").val(resp.data.betSetMoney);

            if ($("#betWith").val() == "percent") {
                $(".setting-profit").attr('class', 'col-12 mt-sp-2 mx-auto text-center setting-profit col-md-5');
                $("#inputBetWithhendle").hide();
            } else if ($("#betWith").val() == "money") {
                $(".setting-profit").attr('class', 'col-12 mt-sp-2 mx-auto text-center setting-profit col-md-4');
                $("#inputBetWithhendle").show();
            }

            GameStart();
        } else {
            killSession();
        }
    }).fail((resp) => {
        killSession();
    });
});

$("#betWith").on("change", () => {
    if ($("#betWith").val() == "percent") {
        $("#inputBetWithhendle").hide();
        $(".setting-profit").attr('class', 'col-12 mt-sp-2 mx-auto text-center setting-profit col-md-5');
    } else if ($("#betWith").val() == "money") {
        $("#inputBetWithhendle").fadeIn();
        $(".setting-profit").attr('class', 'col-12 mt-sp-2 mx-auto text-center setting-profit col-md-4');
    }
});