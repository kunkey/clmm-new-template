$(document).ready(() => {

    $.ajax({
        "url": localStorage.getItem('api') + "/api/user/account",
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {


        if (resp.length == 0) {

        } else {
            window.location = `/setting/account/${resp[0].id}`;
        }


        var i = 1;
        var taskStatus = false;
        for (var data of resp) {
            var body = "<tr>";
            body += `<td>${i}</td>`;
            body += `<td><b>${data.username}</b></td>`;
            body += `<td><b>${data.uid}</b></td>`;
            body += `<td><b>${data.email}</b></td>`;
            var liveStatus = (data.isLive == "true") ? `<button class="btn btn-sm btn-outline-success">Live</button>` : `<button class="btn btn-sm btn-outline-danger">Dead</button>`;
            body += `<td>${liveStatus}</td>`;
            body += `<td>$<b>${data.demoBalance}</b></td>`;
            body += `<td>$<b>${data.realBalance}</b></td>`;
            var betStatus = (data.betStatus == "true") ? `<button class="btn btn-sm btn-outline-success">Bật</button>` : `<button class="btn btn-sm btn-outline-default">Tắt</button>`;
            body += `<td>${betStatus}</td>`;
            body += `<td>${moment(data.createdAt).format("DD/MM/YYYY HH:mm")}</td>`;
            body += `<td>
                        <a href="/setting/account/${data.id}" class="btn btn-sm btn-icon btn-github" data-bs-placement="top" data-toggle="tooltip" data-bs-original-title="Cấu hình tài khoản"><i class="fa fa-sliders"></i></a>
                        <a href="/history/account/${data.id}" class="btn btn-sm btn-icon btn-primary" data-bs-placement="top" data-bs-toggle="tooltip" data-bs-original-title="Lịch sử giao dịch"><i class="fa fa-repeat"></i></a>
                        <button onclick="modalUpdatePassword('${data.id}')" class="btn btn-sm btn-icon btn-dribbble" data-bs-placement="top" data-bs-toggle="tooltip" data-bs-original-title="Cập nhật mật khẩu tài khoản"><i class="fa fa-key"></i></button>
                        <a type="button" onclick="modalDelete('${data.id}')"  class="btn btn-sm btn-icon btn-danger" data-bs-placement="top" data-bs-toggle="tooltip" data-bs-original-title="Xóa tài khoản"><i class="fe fe-trash"></i></a>
                    </td>`;
            body += "</tr>";
            $("#tableAccountBody").append(body);
            i++;
        }
        taskStatus = true;
        if (taskStatus) {
            $('#tableAccount').DataTable({
                language: {
                    searchPlaceholder: 'Tìm Kiếm...',
                    sSearch: '',
                }
            });
        }
    }).fail((resp) => {
        //killSession();
    });
});


const modalUpdatePassword = (id) => {
    $("#initElement").html(`
    <div class="modal fade" id="modalUpdatePassword" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered text-center" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h4 class="modal-title" style="font-weight: bold;">Cập nhật mật khẩu tài khoản sàn giao dịch</h4><button
                        aria-label="Close" class="btn-close" data-bs-dismiss="modal"><span
                            aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" id="addAccountForm">
                    <p><i>* Vui lòng nhập lại mật khẩu ARES BO - điều này sẽ giúp chúng tôi có quyền tự động đăng nhập tài khoản của bạn- tăng khả năng duy trì tài khoản không bị mất kết nối ở phía chúng tôi!</i></p>
                    <div class=" row mb-4"> <label class="col-md-3 form-label">Password</label>
                        <div class="col-md-9">
                            <input type="password" id="passwordAccount" class="form-control"
                                placeholder="Password của tài khoản này">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check"> <input class="form-check-input" type="checkbox" checked id="invalidCheck"
                                required=""> <label class="form-check-label" for="invalidCheck">Tôi chấp nhận để hệ thống can thiệp vào tài khoản của tôi
                                dụng</label>
                            <div class="invalid-feedback">You must agree before submitting.</div>
                        </div>
                    </div>
                    <div class="mb-0 mt-4 row justify-content-end">
                        <div class="col-md-12">
                            <button onclick="updatePasswordAccount(${id})" id="addPasswordBtn" class="btn btn-primary">Cập nhật</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-light" data-bs-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
    `);
    $("#modalUpdatePassword").modal('toggle');
}


const updatePasswordAccount = (id) => {
    Notiflix.Notify.Init({ fontFamily: "Quicksand", useGoogleFont: true });
    var passwordAccount = $("#passwordAccount").val();
    if (!passwordAccount || passwordAccount.length <= 5 || passwordAccount == "" || passwordAccount == "undefined") {
        Notiflix.Notify.Failure("Mật khẩu không hợp lệ!");
        return;
    }
    $("#addPasswordBtn").prop("disabled", true);
    $("#addPasswordBtn").html(`Vui lòng chờ...`);
    $.ajax({
        "url": localStorage.getItem('api') + `/api/user/account/update-password/${id}`,
        "method": "POST",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        "data": JSON.stringify({
            "password": `${passwordAccount}`
        })
    }).done((resp) => {
        $("#addPasswordBtn").prop("disabled", true);
        $("#addPasswordBtn").html(`Cập nhật`);
        $('#modalUpdatePassword').modal('hide');
        Notiflix.Notify.Success("Cập nhật thành công!");
    }).fail((resp) => {
        $("#addPasswordBtn").prop("disabled", false);
        $("#addPasswordBtn").html(`Cập nhật`);
        Notiflix.Notify.Failure("ERROR!");
    });
};


const modalDelete = (id) => {
    Notiflix.Confirm.Show(
        'Bạn có muốn xóa tài khoản này không?',
        'Tất cả dữ liệu của tài khoản sẽ bị loại bỏ hệ thống?',
        'Đồng ý',
        'Hủy bỏ',
        (e) => {

        });
};