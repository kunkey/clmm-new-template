
$("#verifyAccountBtn").click((e) => {
    Notiflix.Notify.Init({ fontFamily: "Quicksand", useGoogleFont: true });
    var code = $("#codeVerifyaddaccount").val();
    var hash = $("#hash").val();
    var token = $("#token").val();
    if(!code || code.length <= 5 || code == "" || code == "undefined") {
      Notiflix.Notify.Failure("Mã xác minh không hợp lệ!");
      return;
    }
    $("#verifyAccountBtn").prop("disabled", true);
    $("#verifyAccountBtn").html(`Vui lòng chờ...`);
  
    $.ajax({
      "url": localStorage.getItem('api') + "/api/ares/verify",
      "method": "POST",
      "dataType": "json",
      "headers": {
        "Access-Control-Allow-Origin": "*",
        "Content-type": "application/json",
        "Authorization": `Bearer ${access_token}` 
      },
      "data": JSON.stringify({
        "hash": `${hash}`,
        "token": `${token}`,
        "code": `${code}`
      }),
    }).done((resp) => {
      $("#verifyAccountBtn").prop("disabled", false);
      $("#verifyAccountBtn").html(`Xác Nhận`);
      if(resp.status) {
        Notiflix.Notify.Success("Thêm tài khoản thành công!");
        setTimeout(() => {
          window.location = "";
        }, 1000);
      }else {
        Notiflix.Notify.Failure("Xác thực thất bại! Vui lòng thử lại!");
      }
    }).fail((resp) => {
      Notiflix.Notify.Failure("SYSTEM ERROR!");
    });
  });
  