const ServerEvents = {
    NEW_USER_JOIN_ROOM: "NEW_USER_JOIN_ROOM",
    ACCEPT_USER_JOIN_ROOM: "ACCEPT_USER_JOIN_ROOM",
    REJECT_USER_JOIN_ROOM: "REJECT_USER_JOIN_ROOM",
    USER_EXIT_ROOM: "USER_EXIT_ROOM",
    USER_TYPING: "USER_TYPING",
    USER_STOP_TYPING: "USER_STOP_TYPING",
    USER_POST_MESSAGE: "USER_POST_MESSAGE",
    CONFIRM_JOIN_ROOM: "CONFIRM_JOIN_ROOM",
    DATA_LOADING: "DATA_LOADING",
    HISTORY_LOADING: "HISTORY_LOADING",
    HISTORY_CHAT: "HISTORY_CHAT",
    AUTHENTICATE: "AUTHENTICATE",
    AUTHENTICATED: "AUTHENTICATED",
    UNAUTHENTICATED: "UNAUTHENTICATED",
    CHANGE_ROUTER: "CHANGE_ROUTER",
    GET_HISTORY_TRADE: "GET_HISTORY_TRADE",
    ANANYTIC_RUNNING: "ANANYTIC_RUNNING",
    ANANYTIC_START_RUN: "ANANYTIC_START_RUN",
    ANANYTIC_COUNT_TRADE: "ANANYTIC_COUNT_TRADE"
};

const ClientEvents = {
    USER_JOIN_ROOM: "USER_JOIN_ROOM", // Event on user join room chat
    USER_EXIT_ROOM: "USER_EXIT_ROOM", // Event on user exit room chat
    USER_TYPING: "USER_TYPING", // Event on user typing message
    USER_STOP_TYPING: "USER_STOP_TYPING",
    USER_POST_MESSAGE: "USER_POST_MESSAGE", // Event on user post new message
    AUTHENTICATE: "AUTHENTICATE",
    GET_INFO: "GET_INFO",
    GET_RUN_STATUS: "GET_RUN_STATUS",
    UPDATE_RUNNING_STATUS: "UPDATE_RUNNING_STATUS",
    FOLLOW_TRADE: "FOLLOW_TRADE",
};

const TradeEvents = {
    ACCOUNT_DEAD: "ACCOUNT_DEAD",
    CONNECT_STATUS: "CONNECT_STATUS",
    TRADE_RESULTS: "TRADE_RESULTS",
    SOCKET_DISCONNECT: "SOCKET_DISCONNECT",
    NEW_ROUND: "NEW_ROUND",
    LAST_ROUND_RESULT: "LAST_ROUND_RESULT",
    PRICE: "PRICE",
    LIMIT_TRADE_REACHED: "LIMIT_TRADE_REACHED",
    COUNT_WIN_LOSE: "COUNT_WIN_LOSE",
    TOTAL_TRADE: "TOTAL_TRADE"
};

const TradeMessages = {
    Connected: "Kết nối thành công!",
    Disconnected: "Kết nối thất bại!",
    AccountDead: "Tài khoản đã chết",
    SOCKET_DISCONNECT: "SOCKET_DISCONNECT",
    NEW_ROUND: "NEW_ROUND",
    LAST_ROUND_RESULT: "LAST_ROUND_RESULT",
    PRICE: "PRICE",
};



const GameStart = async() => {
    var socket = io.connect(localStorage.getItem('api'), {
        rejectUnauthorized: false,
        transports: ["websocket", "polling"],
        forceNew: true,
        query: {
            access_token: localStorage.getItem('access_token'),
            accountID: localStorage.getItem('ID_QUERY')
        }
    });


    // IS CONNECTED TO MAIN SOCKET
    socket.on('connect', () => {
        console.log('connected to socket server!');
        $("#moneyStartRun").html($("#currentmoney").html());
    });

    // AUTHENTICATE STATUS
    socket.on(ClientEvents.AUTHENTICATE, (data) => {
        try {
            if (data.status) {
                console.log("SUCCESS: AUTHENTICATED")
            } else {
                console.log("ERR:  UNAUTHENTICATED")
            }
        } catch (e) {
            console.log(e.message);
        }
    });

    // CHANGE ROUTER TO ACCOUNT SOCKET
    socket.on(ServerEvents.CHANGE_ROUTER, (data) => {
        try {
            console.log(`ROUTER CHANGED: ${localStorage.getItem('api')}/${data.router}`);

            var accountSocket = io.connect(`${localStorage.getItem('api')}/${data.router}`, {
                reconnect: true,
                rejectUnauthorized: false
            });


            // IS CONNECTED TO SOCKET ROUTER
            accountSocket.on('connect', () => {
                console.log('connected to router!');
                //socket.disconnect(); // disconnect in main socket
                try {
                    // chờ init các thông tin xong
                    setTimeout(() => {
                        accountSocket.emit(ClientEvents.GET_INFO, {
                            type: $("#betType").val()
                        });
                    }, 1000);

                    accountSocket.emit(ClientEvents.GET_RUN_STATUS);
                    accountSocket.emit(ClientEvents.FOLLOW_TRADE, { router: data.router });
                } catch (e) {
                    console.log(e.message);
                }
            });

            // GET ACCOUNT INFO
            accountSocket.on(ClientEvents.GET_INFO, (data) => {
                try {
                    const account = data.data;
                    $("#accountUsername").html(account.username);

                    if (data.startAt != 0) {
                        countUpFromTime(data.startAt, "timeRunning");
                    } else {
                        $("#timeRunning").html(`0:0:0:0`);
                    }

                    $("#realBalance").html(`${NumFloor(account.realBalance)}`);
                    $("#demoBlance").html(`${NumFloor(account.demoBalance)}`);

                    if ($("#betType").val() == "LIVE") {
                        $("#currentmoney").html(`${NumFloor(account.realBalance)}`);
                    } else if ($("#betType").val() == "DEMO") {
                        $("#currentmoney").html(`${NumFloor(account.demoBalance)}`);
                    }

                } catch (e) {
                    console.log(e.message);
                }
            });

            // GET RUNNING STATUS
            accountSocket.on(ClientEvents.GET_RUN_STATUS, (data) => {
                try {
                    if (data.running) {
                        $("#betType").prop("disabled", true);
                        $("#betWith").prop("disabled", true);
                        $("#betSetMoney").prop("disabled", true);
                        $("#betLogic").prop("disabled", true);
                        $(".stop-loss-btn").prop('onclick', null).off('click');
                        $(".take-profit-btn").prop('onclick', null).off('click');
                        $("#settingStopBtn").show();
                        $("#analyticContainer").fadeIn();
                    } else {
                        $("#settingUpdateBtn").show();
                    }
                } catch (e) {
                    console.log(e.message);
                }
            });


            // GET HISTORY LIST RESULT TRADE
            accountSocket.on(TradeEvents.TRADE_RESULTS, (data) => {
                try {
                    const trade = data.data;

                    const totalRecords = trade.length;
                    let i = 1;
                    let body = "";
                    trade.forEach((data) => {
                        const classActive = (i == totalRecords) ? "active" : "";
                        if (data.result == "DOWN") {
                            body += `
                            <li class="list-inline-item ${classActive}" data-time="${data.time}" data-bs-toggle="tooltip" data-bs-placement="top" title="${data.timeMoment}"><span class="candle-item fas fa-circle candle-danger">&nbsp;</span></li>`;
                        } else if (data.result == "UP") {
                            body += `
                            <li class="list-inline-item ${classActive}" data-time="${data.time}" data-bs-toggle="tooltip" data-bs-placement="top" title="${data.timeMoment}"><span class="candle-item fas fa-circle candle-success">&nbsp;</span></li>`;
                        }
                        i++;
                    });
                    $("ul.list-prices").html(body);
                } catch (e) {
                    console.log(e.message);
                }
            });

            // PRICE & TIME COUNTDOWN
            accountSocket.on(TradeEvents.PRICE, (data) => {
                try {
                    const trade = data.data;
                    (trade.isBetSession) ? $("#clock-title").html("Hãy đặt lệnh"): $("#clock-title").html("Chờ kết quả");
                    $("#clock-countdown").html(trade.order);
                } catch (e) {
                    console.log(e.message);
                }
            });


            // UPDATE HISTOORY TRADE
            accountSocket.on(ServerEvents.GET_HISTORY_TRADE, (dataSocket) => {
                try {
                    var i = 1;
                    var taskStatus = false;
                    var body = "";
                    for (var data of dataSocket.data) {
                        var balanceType = (data.balanceType == "LIVE") ? "THẬT" : "DEMO";
                        var betChoice = (data.betChoice == "UP") ? `MUA` : `BÁN`;
                        var betResult = (data.betResult == "WIN") ? `<button class="btn btn-sm btn-outline-success">THẮNG</button>` : (data.betResult == "LOSE") ? `<button class="btn btn-sm btn-outline-danger">THUA</button>` : `<i class="fas fa-spinner fa-spin"></i>`;
                        var betWin = (data.betWin == null) ? `<i class="fas fa-spinner fa-spin"></i>` : `$ <b>${NumFloor(data.betWin)}</b>`;
                        var betLost = (data.betLost == null) ? `<i class="fas fa-spinner fa-spin"></i>` : `$ <b>${NumFloor(data.betLost)}</b>`;

                        var profits;
                        if (data.betWin == null && data.betLost == null) {
                            profits = `<i class="fas fa-spinner fa-spin"></i>`;
                        } else if (data.betWin != 0) {
                            const cacProfit = Number(data.betWin) - Number(data.betMoney);
                            profits = `<b style="font-weight: 700; color: #0deb00;">${NumFloor(cacProfit)}$</b>`;
                        } else if (data.betLost != 0) {
                            profits = `<b style="font-weight: 700; color: #f74343;">-${NumFloor(data.betLost)}$</b>`;
                        }

                        body += "<tr>";
                        body += `<td>${i}</td>`;
                        body += `<td><b>${data.roundId}</b></td>`;
                        body += `<td><b>${balanceType}</b></td>`;
                        body += `<td><b>${betChoice}</b></td>`;
                        body += `<td>$ <b>${NumFloor(data.betMoney)}</b></td>`;
                        body += `<td>${betResult}</td>`;
                        body += `<td>${profits}</td>`;
                        body += `<td>${moment(data.createdAt).format("DD/MM/YYYY HH:mm")}</td>`;
                        body += "</tr>";
                        i++;
                    }

                    taskStatus = true;
                    if (taskStatus) {
                        $("#historyAccountBody").html(body);
                        // $('#historyAccount').DataTable({
                        //     language: {
                        //         searchPlaceholder: 'Tìm Kiếm...',
                        //         sSearch: '',
                        //     }
                        // });
                    }
                } catch (e) {
                    console.log(e.message);
                }
            });

            // UPDATE ANANYTIC_START_RUN
            accountSocket.on(ServerEvents.ANANYTIC_START_RUN, (dataSocket) => {
                try {
                    const data = dataSocket.data;

                    if (data !== null) {
                        setTimeout(() => {
                            $("#moneyStartRun").html(`${NumFloor(data.betMoney)}`);
                            const profits = Number($("#currentmoney").html()) - Math.floor(data.betMoney);
                            $("#profits").html(`${Math.floor(profits)}`);
                        }, 2000);
                        $("#betChoice").html((data.betChoice == "UP") ? "<div class=\"zoom-in-zoom-out\">MUA</div>" : "<div class=\"zoom-in-zoom-out\">BÁN</div>");
                    } else {
                        $("#moneyStartRun").html($("#currentmoney").html());
                    }
                } catch (e) {
                    console.log(e.message);
                }
            });

            // UPDATE ANANYTIC MONEY, BLANCE, ALI, PROFIT, TAKE MONEY....
            accountSocket.on(ServerEvents.ANANYTIC_RUNNING, (dataSocket) => {
                try {
                    const data = dataSocket.data;
                    if ($("#betType").val() == "LIVE") {
                        $("#balance").html(`$ ${NumFloor(data.realBalance)}`);
                    } else if ($("#betType").val() == "DEMO") {
                        $("#balance").html(`$ ${NumFloor(data.demoBalance)}`);
                    }
                } catch (e) {
                    console.log(e.message);
                }
            });

            // UPDATE ANANYTIC MONEY, BLANCE, ALI, PROFIT, TAKE MONEY....
            accountSocket.on(ServerEvents.ANANYTIC_COUNT_TRADE, (dataSocket) => {
                try {
                    const data = dataSocket.data;
                    $("#currentPercent").html(`${dataSocket.currentPercent}%`);
                    $("#betingMoney").html(NumFloor(dataSocket.betingMoney));
                    const result = (data.betChoice == data.sessionResult) ? "WIN" : "LOSE";
                    $("#currentResult").html(`<div class="zoom-in-zoom-out">${result}</div>`);
                } catch (e) {
                    console.log(e.message);
                }
            });

            accountSocket.on(TradeEvents.COUNT_WIN_LOSE, (dataSocket) => {
                try {
                    if (dataSocket.data) {
                        $("#winCount").html(dataSocket.data.totalWin);
                        $("#loseCount").html(dataSocket.data.totalLost);
                    }
                } catch (e) {
                    console.log(e.message);
                }
            });

            accountSocket.on(TradeEvents.TOTAL_TRADE, (dataSocket) => {
                try {
                    const data = dataSocket.data;
                    $("#totalTradeMoney").html(`<i class="fas fa-dollar-sign"></i> ${NumFloor(data)}`);
                } catch (e) {
                    console.log(e.message);
                }
            });


            accountSocket.on(TradeEvents.LIMIT_TRADE_REACHED, (dataSocket) => {
                try {
                    const data = dataSocket.data;
                    modalLimitReached(data);
                } catch (e) {
                    console.log(e.message);
                }
            });

            // ACCOUNT_DEAD
            accountSocket.on(TradeEvents.ACCOUNT_DEAD, (data) => {
                try {
                    console.log("ERR:  " + `${data}`);
                    modalDeadAccount();
                } catch (e) {
                    console.log(e.message);
                }
            });

            // END SOCKET ROUTER //

            // CHANGE ACCOUNT TYPE BY CLIENT
            $("#betType").on("change", () => {
                try {
                    accountSocket.emit(ClientEvents.GET_INFO, {
                        type: $("#betType").val()
                    });
                } catch (e) {
                    console.log(e.message);
                }
            });

            // END CHANGE ROUTER
        } catch (e) {
            console.log(e.message);
        }
    });

    // ACCOUNT_DEAD
    socket.on(TradeEvents.ACCOUNT_DEAD, (data) => {
        console.log("ERR:  " + `${data}`);
        modalDeadAccount();
    });
}



const modalDeadAccount = () => {
    $("#modalNotifyContainer").html(`
        <div class="modal show" id="modaldeadAcc" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered text-center" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h4 class="modal-title" style="font-weight: bold;">Thông báo!</h4><button
                            aria-label="Close" class="btn-close" data-bs-dismiss="modal"><a href="/"><span
                                aria-hidden="true">×</span></a></button>
                    </div>
                    <div class="modal-body" id="addAccountForm">
                        <p><i>* Tài khoản của bạn đã chết vui lòng đăng nhập lại tài khoản để tiếp tục!</i></p>
                    </div>
                    <div class="modal-footer">
                        <a href="/"><button class="btn btn-light" data-bs-dismiss="modal">Đóng</button></a>
                    </div>
                </div>
            </div>
        </div>
    `);
    $("#modaldeadAcc").modal('toggle');
}


const modalLimitReached = (data) => {
    const currentMoney = $("#currentmoney").html();
    const betWinMoney = (data.profit > 0) ? data.profit : 0;
    const betLoseMoney = (data.profit < 0) ? data.profit.slice(1) : 0;

    $("#modalNotifyContainer").html(`
        <div class="modal show" id="modalLimitReached" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered text-center" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h4 class="modal-title" style="font-weight: bold;">Hoàn thành công việc!</h4>
                        <button aria-label="Close" class="btn-close" data-bs-dismiss="modal">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body" id="addAccountForm">
                        <p>* Quá trình giao dịch tự động đã kết thúc với giới hạn giao dịch mà bạn đặt ra!</p>
                        <p>* Thống kê lại quá trình giao dịch vừa kết thúc:</p>
                        <br>
                        <div class="card-body" style="background-color: #282840;">
                            <div class="clearfix row mb-4">
                                <div class="col">
                                    <div class="float-start">
                                        <h5 class="mb-0"><b style="font-weight:700;"><i class="fas fa-donate">&nbsp;</i> Số tiền ban đầu:</b></h5>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="float-end">
                                        <h4 class="fw-bold mb-0 text-blue">$${Math.floor(data.betMoney)}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix row mb-4">
                                <div class="col">
                                    <div class="float-start">
                                        <h5 class="mb-0"><b style="font-weight:700;"><i class="fas fa-donate">&nbsp;</i> Số tiền hiện tại:</b></h5>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="float-end">
                                        <h4 class="fw-bold mb-0 text-info" id="#reportCurrentMoney">$${Math.floor(currentMoney)}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix row mb-4">
                                <div class="col">
                                    <div class="float-start">
                                        <h5 class="mb-0"><b style="font-weight:700;"><i class="fas fa-donate">&nbsp;</i> Tiền lãi:</b></h5>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="float-end">
                                        <h4 class="fw-bold mb-0 text-success">$${Math.floor(betWinMoney)}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix row mb-4">
                                <div class="col">
                                    <div class="float-start">
                                        <h5 class="mb-0"><b style="font-weight:700;"><i class="fas fa-donate">&nbsp;</i> Tiền lỗ:</b></h5>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="float-end">
                                        <h4 class="fw-bold mb-0 text-danger">$${Math.floor(betLoseMoney)}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix row mb-4">
                                <div class="col">
                                    <div class="float-start">
                                        <h5 class="mb-0"><b style="font-weight:700;"><i class="fas fa-donate">&nbsp;</i> Số lần bet thắng:</b></h5>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="float-end">
                                        <h4 class="fw-bold mb-0 text-info" id="#reportBetWinCount">?</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix row mb-4">
                                <div class="col">
                                    <div class="float-start">
                                        <h5 class="mb-0"><b style="font-weight:700;"><i class="fas fa-donate">&nbsp;</i> Số lần bet thua:</b></h5>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="float-end">
                                        <h4 class="fw-bold mb-0 text-warning" id="#reportBetLoseCount">?</h4>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-light" data-bs-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
    `);
    $("#modalLimitReached").modal('toggle');
}