$("#verify2FaAccountBtn").click((e) => {
    Notiflix.Notify.Init({ fontFamily: "Quicksand", useGoogleFont: true });
    var code = $("#codeVerifyaddaccount").val();
    var hash = $("#hash").val();
    if (!code || code.length <= 5 || code == "" || code == "undefined") {
        Notiflix.Notify.Failure("Mã xác minh 2FA không hợp lệ!");
        return;
    }
    $("#verify2FaAccountBtn").prop("disabled", true);
    $("#verify2FaAccountBtn").html(`Vui lòng chờ...`);

    $.ajax({
        "url": localStorage.getItem('api') + "/api/ares/verify2fa",
        "method": "POST",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        "data": JSON.stringify({
            "hash": `${hash}`,
            "code": `${code}`
        }),
    }).done((resp) => {
        $("#verify2FaAccountBtn").prop("disabled", false);
        $("#verify2FaAccountBtn").html(`Xác Nhận`);
        if (resp.status) {
            Notiflix.Notify.Success("Thêm tài khoản thành công!");
            setTimeout(() => {
                window.location = "";
            }, 1000);
        } else {
            Notiflix.Notify.Failure("Xác thực thất bại! Vui lòng thử lại!");
        }
    }).fail((resp) => {
        Notiflix.Notify.Failure("SYSTEM ERROR!");
    });
});