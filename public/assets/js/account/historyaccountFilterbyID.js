const filter = () => {
    const startAt = $('input[name="datepickerStart"]').val().trim();
    const endAt = $('input[name="datepickerEnd"]').val().trim();

    $.ajax({
        "url": localStorage.getItem('api') + `/api/user/account/static/${localStorage.getItem('ID_QUERY')}?fromDate=${startAt}&toDate=${endAt}`,
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {

        $("#historyProfits").html(`${resp.data.profits}`);
        console.log(resp.data.profits);
        $("#wincount").html(`${resp.data.winCount}`);
        $("#losemoney").html(`$${resp.data.loseMoney}`);
    }).fail((resp) => {
        //killSession();
    });
}