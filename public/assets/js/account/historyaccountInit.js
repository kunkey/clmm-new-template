$(document).ready(() => {
    filter();
    var dateInputStart = $('input[name="datepickerStart"]');
    var dateInputEnd = $('input[name="datepickerEnd"]');
    var options = {
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
    };
    dateInputStart.datepicker(options);
    dateInputEnd.datepicker(options);
    dateInputStart.change(function() {
        filter();
    });
    dateInputEnd.change(function() {
        filter();
    });
});

$(document).ready(() => {
    $.ajax({
        "url": localStorage.getItem('api') + `/api/user/account/history/${localStorage.getItem('ID_QUERY')}`,
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {
        var i = 1;
        var taskStatus = false;
        for (var data of resp.data) {

            var profits = (data.profit > 0) ? `<b style="font-weight: 700; color: #0deb00;">${NumFloor(data.profit)}$</b>` : `<b style="font-weight: 700; color: #f74343;">${NumFloor(data.profit)}$</b>`;

            var body = "<tr>";
            body += `<td>${i}</td>`;
            body += `<td><b>#${data.tradeId}</b></td>`;
            body += `<td><b>${NumFloor(data.startMoney)}$</b></td>`;
            body += `<td><b>${profits}</b></td>`;
            body += `<td>${data.totalWin} lần</td>`;
            body += `<td>${data.totalLose} lần</td>`;
            body += `<td>${moment(data.createdAt).format("DD/MM/YYYY HH:mm")}</td>`;
            body += "</tr>";

            $("#historyAccountBody").append(body);
            i++;
        }
        taskStatus = true;
        if (taskStatus) {
            $('#historyAccount').DataTable({
                language: {
                    searchPlaceholder: 'Tìm Kiếm...',
                    sSearch: '',
                }
            });
        }
    }).fail((resp) => {
        killSession();
    });
});