const changePassword = () => {
    Notiflix.Notify.Init({ fontFamily: "Quicksand", useGoogleFont: true });
    var oldpassword = $("#oldpassword").val();
    var newpassword = $("#newpassword").val();
    var confirmpassword = $("#confirmpassword").val();


    if (!oldpassword || oldpassword == "" || oldpassword == "undefined" || !newpassword || newpassword == "" || newpassword == "undefined" || !confirmpassword || confirmpassword == "" || confirmpassword == "undefined") {
        Notiflix.Notify.Failure("Vui lòng nhập đầy đủ thông tin!");
        return;
    }

    if (!oldpassword || oldpassword.length <= 5) {
        Notiflix.Notify.Failure("Mật khẩu cũ không hợp lệ!");
        return;
    }

    if (!newpassword || newpassword.length <= 5) {
        Notiflix.Notify.Failure("Mật khẩu mới không được nhỏ hơn 5 kí tự!");
        return;
    }

    if (newpassword != confirmpassword) {
        Notiflix.Notify.Failure("2 mật khẩu bạn vừa nhập không khớp!");
        return;
    }


    $("#btnChangePassword").prop("disabled", true);
    $("#btnChangePassword").html(`Vui lòng chờ...`);

    $.ajax({
        "url": localStorage.getItem('api') + "/api/auth/update-password",
        "method": "PUT",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        "data": JSON.stringify({
            "oldPassword": `${oldpassword}`,
            "password": `${newpassword}`
        }),
    }).done((resp) => {
        $("#btnChangePassword").prop("disabled", false);
        $("#btnChangePassword").html(`Cập nhật`);
        if (resp.status) {
            Notiflix.Notify.Success("Đổi mật khẩu thành công!");
            setTimeout(() => {}, 1000);
        } else {
            if (resp.message == "missing_field_list") {
                Notiflix.Notify.Failure("Lỗi dữ liệu!");
            } else {
                Notiflix.Notify.Failure("Mật khẩu cũ bạn vừa nhập không chính xác!");
            }
        }
    }).fail((resp) => {
        $("#btnChangePassword").prop("disabled", false);
        $("#btnChangePassword").html(`Cập nhật`);
        Notiflix.Notify.Failure("Mật khẩu cũ bạn vừa nhập không chính xác!");
    });
};