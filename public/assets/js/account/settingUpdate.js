$("#settingUpdateBtn").on("click", () => {
    if ($("#betWith").val() == "money") {
        if (Number($("#betSetMoney").val()) < 1) {
            Notiflix.Notify.Failure("Số tiền mỗi lệnh phải lớn hơn 0!");
            return;
        }
    }
    $("#settingUpdateBtn").prop("disabled", true);
    $("#settingUpdateBtn").html("vui lòng chờ...");

    $.ajax({
        "url": localStorage.getItem('api') + `/api/user/account/setting/${localStorage.getItem('ID_QUERY')}`,
        "method": "POST",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        "data": JSON.stringify({
            "betStatus": $("#betStatus").prop("checked"),
            "betType": $("#betType").val(),
            "betMoney": $("#betMoney").val(),
            "betWinMax": Number($("#bot_take_profit").val()),
            "betLoseMax": Number("-" + $("#bot_stop_loss").val()),
            "betLogic": $("#betLogic").val(),
            "betTypeTrade": $("#betWith").val(),
            "betSetMoney": ($("#betWith").val() == "money") ? $("#betSetMoney").val() : 0
        }),
    }).done((resp) => {
        if (resp.status) {
            $("#settingUpdateBtn").prop("disabled", false);
            $("#settingUpdateBtn").html(`<span class="fas fa-play-circle bot-status-icon" aria-hidden="true">&nbsp;</span> Chạy lại`);
            Notiflix.Notify.Success("Cấu hình tài khoản thành công!");
            window.location = "";
        } else {
            $("#settingUpdateBtn").prop("disabled", false);
            $("#settingUpdateBtn").html(`<span class="fas fa-play-circle bot-status-icon" aria-hidden="true">&nbsp;</span> Chạy Auto`);
            Notiflix.Notify.Failure(resp.message);
        }
    }).fail((resp) => {
        killSession();
    });
});

$("#settingStopBtn").on("click", () => {
    $("#settingStopBtn").prop("disabled", true);
    $("#settingStopBtn").html("vui lòng chờ...");

    $.ajax({
        "url": localStorage.getItem('api') + `/api/user/account/stop/${localStorage.getItem('ID_QUERY')}`,
        "method": "POST",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        "data": JSON.stringify({
            "betStatus": false
        }),
    }).done((resp) => {
        if (resp.status) {
            $("#settingStopBtn").prop("disabled", false);
            $("#settingStopBtn").html(`<span class="fas fa-play-circle bot-status-icon" aria-hidden="true">&nbsp;</span> Chạy lại`);
            Notiflix.Notify.Success("Cấu hình tài khoản thành công!");
            window.location = "";
        } else {
            $("#settingStopBtn").prop("disabled", false);
            $("#settingStopBtn").html(`<span class="fas fa-play-circle bot-status-icon" aria-hidden="true">&nbsp;</span> Chạy Auto`);
            Notiflix.Notify.Failure(resp.message);
        }
    }).fail((resp) => {
        killSession();
    });
});

$(document).ready(() => {
    $("#modalNotifyContainer").html(`
    <div class="modal show" id="modaldeadAcc" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered text-center" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h4 class="modal-title" style="font-weight: bold;">Cài đặt cắt lỗ / chốt lãi</h4>
                    <button aria-label="Close" class="btn-close" data-bs-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" id="">
                    <div class=" row mb-4">
                    <label class="col-md-3 form-label">Cắt lỗ</label>
                        <div class="col-md-9">
                            <input type="number" id="bot_stop_loss" class="form-control" value="0" placeholder="Vui lòng nhập vào số lớn hơn 0">
                        </div>
                    </div>
                    <div class=" row mb-4">
                        <label class="col-md-3 form-label" for="example-email">Chốt lãi</label>
                        <div class="col-md-9">
                            <input type="number" id="bot_take_profit" class="form-control" value="0" placeholder="Vui lòng nhập vào số lớn hơn 0">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button onclick="profitSettingUpdate()" class="btn btn-primary"><span class="fas fa-save" aria-hidden="true">&nbsp;</span> Lưu lại</button>
                    <button class="btn btn-light" data-bs-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
    `);
});


const profitSettingForm = () => {
    $("#modaldeadAcc").modal('toggle');
}

const profitSettingUpdate = () => {
    try {
        if (!$("#bot_take_profit").val() || !$("#bot_stop_loss").val()) {
            Notiflix.Notify.Failure("Vui lòng nhập vào mức chốt lỗ chốt lãi!");
            return;
        }

        if (Number($("#bot_take_profit").val()) < 0) {
            Notiflix.Notify.Failure("Mức chốt lãi phải lớn hơn 0!");
            return;
        }
        if (Number($("#bot_stop_loss").val()) < 0) {
            Notiflix.Notify.Failure("Mức chốt lỗ  phải lớn hơn 0!");
            return;
        }
        $(".stop-loss").html(`${$("#bot_stop_loss").val()}`);
        $(".take-profit").html(`${$("#bot_take_profit").val()}`);
        Notiflix.Notify.Success("Cấu hình thành công!");
        $("#modaldeadAcc").modal('hide');
    } catch (e) {
        Notiflix.Notify.Failure("Vui lòng nhập vào giá trị hợp lệ!");
    }
}



$(document).ready(() => {
    $("#exchangeMoney").on("click", () => {

        $("#modalAccount").html(`
                <div class="modal show" id="modalAccountHandle" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered text-center" role="document">
                        <div class="modal-content modal-content-demo">
                            <div class="modal-header">
                                <h4 class="modal-title" style="font-weight: bold;">Chuyển USDT Wallet Sang Tài Khoản Thực!</h4>
                                <button aria-label="Close" class="btn-close" data-bs-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body" id="">
                                <i>* Sau khi chuyển xong bạn vui lòng tải lại trang<br>để số dư được cập nhật!</i><br>
                                <i>* Sau khi chuyển có thể hệ thống sẽ cập nhật chậm, vui lòng báo lỗi với admin nếu sau 30s mà vẫn chưa thấy thành công!</i>
                                <div class="input-group" style="margin-top:10px;">
                                    <span class="input-group-text btn btn-primary"><i class="fas fa-hand-holding-usd"></i></span>
                                    <input type="text" id="usdt_amount" readonly placeholder="USDT Wallet" class="form-control"> 
                                    <input type="text" id="real_amount" readonly placeholder="Tài khoản thực" class="form-control">
                                    <span class="input-group-text btn btn-primary"><i class="fas fa-dollar-sign"></i></span>
                                </div>

                                <div class="row" style="margin-top:20px;">
                                <div class="col-lg-7 col-xs-5 col-md-7 col-sm-12 center-block">
                                  <div class="input-group">
                                    <span class="input-group-text btn btn-primary"><i class="fas fa-funnel-dollar"></i></span>
                                    <input type="number" id="transferMoney" class="form-control" placeholder="Nhập số tiền cần chuyển">
                                  </div>
                                  <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                              </div>
                                <div class="text-center" style="margin-top:20px;">
                                        <button id="btnTransfer" onclick="exchangeMoney()" class="btn btn-primary"><span class="fas fa-exchange-alt" aria-hidden="true">&nbsp;</span> Chuyển</button>
                                </div>
                                <center>
                                </center>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-light" data-bs-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
                `);

        $("#modalAccountHandle").modal('show');
        $.ajax({
            "url": localStorage.getItem('api') + `/api/user/account/getbalance/${localStorage.getItem('ID_QUERY')}`,
            "method": "GET",
            "dataType": "json",
            "headers": {
                "Access-Control-Allow-Origin": "*",
                "Content-type": "application/json",
                "Authorization": `Bearer ${access_token}`
            },
        }).done((resp) => {
            if (resp.status) {
                $("#usdt_amount").val(`${Number(resp.data.usdtBalance)} USDT`);
                $("#real_amount").val(`${Number(resp.data.realBalance)} $`);
            } else {
                Notiflix.Notify.Failure("ERROR: Lỗi truy vấn tài khoản! vui lòng thử  lại!");
            }
        }).fail((resp) => {
            Notiflix.Notify.Failure("ERROR: Lỗi truy vấn tài khoản! vui lòng thử  lại!");
        });
    });
});


const exchangeMoney = async() => {
    try {

        if (!$("#transferMoney").val()) {
            Notiflix.Notify.Failure(`Vui lòng nhập vào số tiền cần chuyển!`);
            return;
        }

        if (Number($("#transferMoney").val()) < 1) {
            Notiflix.Notify.Failure(`Số tiền vần chuyển phải lớn hơn 0!`);
            return;
        }

        $("#btnTransfer").prop("disabled", true);
        $("#btnTransfer").html("<i class='fas fa-spinner fa-spin'></i> Đang thực hiện", true);

        $.ajax({
            "url": localStorage.getItem('api') + `/api/user/account/transfer/${localStorage.getItem('ID_QUERY')}`,
            "method": "POST",
            "dataType": "json",
            "headers": {
                "Access-Control-Allow-Origin": "*",
                "Content-type": "application/json",
                "Authorization": `Bearer ${access_token}`
            },
            "data": JSON.stringify({
                "amount": NumFloor($("#transferMoney").val())
            }),
        }).done((resp) => {
            if (resp.status) {
                $("#btnTransfer").prop("disabled", false);
                $("#btnTransfer").html(`<span class="fas fa-exchange-alt" aria-hidden="true">&nbsp;</span> Chuyển`, true);
                Notiflix.Notify.Success(`${resp.message}`);
                setTimeout(() => {
                    window.location.reload();
                }, 2000);
            } else {
                $("#btnTransfer").prop("disabled", false);
                $("#btnTransfer").html(`<span class="fas fa-exchange-alt" aria-hidden="true">&nbsp;</span> Chuyển`, true);
                Notiflix.Notify.Failure(resp.message);
            }
        }).fail((resp) => {
            Notiflix.Notify.Failure(`ERROR: `)
            $("#btnTransfer").prop("disabled", false);
            $("#btnTransfer").html(`<span class="fas fa-exchange-alt" aria-hidden="true">&nbsp;</span> Chuyển`, true);
        });
    } catch (e) {
        Notiflix.Notify.Failure(`ERROR: ${e.message}`);
        $("#btnTransfer").prop("disabled", false);
        $("#btnTransfer").html(`<span class="fas fa-exchange-alt" aria-hidden="true">&nbsp;</span> Chuyển`, true);
    }
};