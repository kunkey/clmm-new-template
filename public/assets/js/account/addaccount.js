$("#addAccountBtn").click((e) => {
    retryAccount();
});


const retryAccount = () => {
    Notiflix.Notify.Init({ fontFamily: "Quicksand", useGoogleFont: true });
    var email = $("#emailaddaccount").val();
    var password = $("#passwordaddaccount").val();
    if (!email || email.length <= 5 || email == "" || email == "undefined") {
        Notiflix.Notify.Failure("Vui lòng nhập đầy đủ thông tin!");
        return;
    }
    if (!password || password == "" || password == "undefined") {
        Notiflix.Notify.Failure("Vui lòng nhập đầy đủ thông tin!");
        return;
    }
    $("#addAccountBtn").prop("disabled", true);
    $("#addAccountBtn").html(`Vui lòng chờ...`);

    $.ajax({
        "url": localStorage.getItem('api') + "/api/ares/login",
        "method": "POST",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        "data": JSON.stringify({
            "email": `${email}`,
            "password": `${password}`
        }),
    }).done((resp) => {
        $("#addAccountBtn").prop("disabled", false);
        $("#addAccountBtn").html(`Thêm tài khoản`);
        if (resp.status) {
            Notiflix.Notify.Success("Thêm tài khoản thành công!");
            setTimeout(() => {
                window.location = "";
            }, 1000);
        } else {

            if (resp.message == "invalid-captcha") {
                retryAccount();
            } else if (resp.message == "verify_device") {
                $("#addAccountForm").html('');
                $("#addAccountForm").append(`<input type="hidden" id="hash" value="${resp.hash}">`);
                $("#addAccountForm").append(`<input type="hidden" id="token" value="${resp.token}">`);
                $("#addAccountForm").append(`
                            <p><i>* Vui lòng nhập mã xác minh sàn giao dịch gửi vào email của bạn!</i></p>
                            <div class=" row mb-4"> <label class="col-md-3 form-label" for="example-email">Mã Xác Minh</label>
                                <div class="col-md-9">
                                    <input type="text" id="codeVerifyaddaccount" class="form-control" placeholder="Nhập mã xác minh đăng nhập">
                                </div>
                                <div class="mb-0 mt-4 row justify-content-end">
                                    <div class="col-md-12">
                                        <button id="verifyAccountBtn" class="btn btn-primary">Xác Nhận</button>
                                    </div>
                                </div>
                            </div>
                            `);
                $("#addAccountForm").append(`<script src="assets/js/account/verifyaccount.js"></script>`);
            } else if (resp.message == "require2Fa") {
                $("#addAccountForm").html('');
                $("#addAccountForm").append(`<input type="hidden" id="hash" value="${resp.hash}">`);
                $("#addAccountForm").append(`
                            <p><i>* Vui lòng nhập mã xác minh 2FA  Code của bạn trong ứng dụng Google Authenticator!</i></p>
                            <div class=" row mb-4"> <label class="col-md-3 form-label" for="example-email">Mã Xác Minh 2FA</label>
                                <div class="col-md-9">
                                    <input type="text" id="codeVerifyaddaccount" class="form-control" placeholder="Nhập mã xác minh 2FA">
                                </div>
                                <div class="mb-0 mt-4 row justify-content-end">
                                    <div class="col-md-12">
                                        <button id="verify2FaAccountBtn" class="btn btn-primary">Xác Nhận</button>
                                    </div>
                                </div>
                            </div>
                            `);
                $("#addAccountForm").append(`<script src="assets/js/account/verify2Faaccount.js"></script>`);
            } else if (resp.message == "incorrect-email-password") {
                Notiflix.Notify.Failure("Tài khoản hoặc mật khẩu không chính xác!");
            } else if (resp.message == "your_account_is_blocked") {
                Notiflix.Report.Failure(
                    'Phát hiện nghi vấn',
                    'Hệ thống phát hiện thấy tài khoản bạn vừa đăng nhập không khớp với tên tài khoản lúc đăng kí! Hệ thống đã khóa tài khoản của bạn để kiểm tra! Vui lòng liên hệ với Admin để được mở khóa!',
                    'Đóng'
                );
            } else {
                Notiflix.Notify.Failure(resp.message);
            }
        }

    }).fail((resp) => {
        Notiflix.Notify.Failure("SYSTEM ERROR!");
    });
};