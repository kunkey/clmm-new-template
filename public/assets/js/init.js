var access_token = localStorage.getItem('access_token');

if (access_token == "" || access_token == null || access_token == "undefined") window.location = "/login";

const killSession = () => {
    localStorage.clear();
    window.location = "/logout";
};

$.ajax({
    "url": localStorage.getItem('api') + "/api/auth/me",
    "method": "GET",
    "dataType": "json",
    "headers": {
        "Access-Control-Allow-Origin": "*",
        "Content-type": "application/json",
        "Authorization": `Bearer ${access_token}`
    },
}).done((resp) => {
    $("#username").html(resp.user.name.toUpperCase());
    $("#role").html(resp.user.role.toUpperCase());

    if (resp.user.verify == "false") {
        window.location = "/verify";
    } else if (resp.user.status == "pending") {
        window.location = "/access-fail";
    } else if (resp.user.status == "blocked") {
        window.location = "/account-blocked";
    }
}).fail((resp) => {
    killSession();
});

const countUpFromTime = (countFrom, id) => {
    countFrom = new Date(countFrom).getTime();
    var now = new Date(),
        countFrom = new Date(countFrom),
        timeDifference = (now - countFrom);

    var secondsInADay = 60 * 60 * 1000 * 24,
        secondsInAHour = 60 * 60 * 1000;

    days = Math.floor(timeDifference / (secondsInADay) * 1);
    hours = Math.floor((timeDifference % (secondsInADay)) / (secondsInAHour) * 1);
    mins = Math.floor(((timeDifference % (secondsInADay)) % (secondsInAHour)) / (60 * 1000) * 1);
    secs = Math.floor((((timeDifference % (secondsInADay)) % (secondsInAHour)) % (60 * 1000)) / 1000 * 1);

    var idEl = document.getElementById(id);
    idEl.innerHTML = `${days}:${hours}:${mins}:${secs}`;

    clearTimeout(countUpFromTime.interval);
    countUpFromTime.interval = setTimeout(() => {
        countUpFromTime(countFrom, id);
    }, 1000);
}

const NumFloor = (num) => {
    return Math.floor(num * 100) / 100;
}


const logoutBtn = () => {
    Notiflix.Confirm.Show(
        'Bạn có muốn đăng xuất không?',
        'Hệ thống vẫn sẽ chạy khi bạn thoát?',
        'Đồng ý',
        'Hủy bỏ',
        (e) => {
            window.location = "/logout"
        });
};


// Used to detect whether the users browser is an mobile browser
function isMobile() {
    ///<summary>Detecting whether the browser is a mobile browser or desktop browser</summary>
    ///<returns>A boolean value indicating whether the browser is a mobile browser or not</returns>

    if (sessionStorage.desktop) // desktop storage
        return false;
    else if (localStorage.mobile) // mobile storage
        return true;

    // alternative
    var mobile = ['iphone', 'ipad', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile)
        if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) return true;

        // nothing found.. assume desktop
    return false;
}