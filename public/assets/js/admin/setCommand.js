$("#setCommandBtn").on("click", () => {
    if(!$("#money").val() || $("#money").val().length == 0) {
        Notiflix.Notify.Failure("Vui lòng nhập đầy đủ thông tin!");
        return;
    }

    $("#setCommandBtn").prop("disabled", true);
    $("#setCommandBtn").html(`Vui lòng chờ...`);

    $.ajax({
        "url": localStorage.getItem('api') + `/api/admin/ares/set-command/bet`,
        "method": "POST",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        "data": JSON.stringify({
            "type": $("#type").val(),
            "command": $("#command").val(),
            "money": $("#money").val()
        }),
    }).done((resp) => {
        $("#successCount").html(resp.data.success);
        $("#errorCount").html(resp.data.error);
        $("#setCommandBtn").prop("disabled", false);
        $("#setCommandBtn").html(`Vào Lại`);
        Notiflix.Notify.Success("Vào Lệnh Hoàn Tất!");
    }).fail((resp) => {
        killSession();
    });
});