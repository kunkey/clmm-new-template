$(document).ready(() => {
    $.ajax({
        "url": localStorage.getItem('api') + `/api/admin/dealer`,
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {
        var i = 1;
        var taskStatus = false;
        for (var data of resp) {
            var body = "<tr>";
            body += `<td>${i}</td>`;
            body += `<td><b>${data.username}</b></td>`;
            body += `<td><b>${data.email}</b></td>`;
            var userStatus = (data.status == "active") ? `<button class="btn btn-sm btn-outline-success">Hoạt Động</button>` : `<button class="btn btn-sm btn-outline-danger">Không Hoạt Động</button>`;
            body += `<td>${userStatus}</td>`;
            var userVerify = (data.verify == "true") ? `<button class="btn btn-sm btn-outline-success">Đã xác minh</button>` : `<button class="btn btn-sm btn-outline-danger">Chưa xác minh</button>`;
            body += `<td><b>${userVerify}</b></td>`;
            body += `<td>${moment(data.createdAt).format("DD/MM/YYYY HH:mm")}</td>`;
            body += `<td>
                <button type="button" title="Xóa" onclick="modalDelete(${data.id})" class="btn btn-icon  btn-danger"><i class="fe fe-trash"></i></button></td>
                `;
            body += "</tr>";
            $("#dealerListBody").append(body);
            i++;
        }
        taskStatus = true;
        if (taskStatus) {
            $('#dealerList').DataTable({
                language: {
                    searchPlaceholder: 'Tìm Kiếm...',
                    sSearch: '',
                }
            });
        }
    }).fail((resp) => {
        killSession();
    });
});




const modalDelete = (id) => {
    Notiflix.Confirm.Show(
        'Bạn có muốn xóa tài khoản này không?',
        'Tất cả dữ liệu của tài khoản sẽ bị loại bỏ hệ thống?',
        'Đồng ý',
        'Hủy bỏ',
        (e) => {
            $.ajax({
                "url": localStorage.getItem('api') + `/api/admin/dealer/delete`,
                "method": "POST",
                "dataType": "json",
                "headers": {
                    "Access-Control-Allow-Origin": "*",
                    "Content-type": "application/json",
                    "Authorization": `Bearer ${access_token}`
                },
                "data": JSON.stringify({
                    "id": id
                }),
            }).done((resp) => {
                window.location.reload();
            }).fail((resp) => {
                window.location.reload();
            });
        });
};