const initModalAddDealer = () => {
    $("#modalDealerTittle").html(`Tạo Tài Khoản Đại Lý`);
    $("#modalDealerFooter").html(`
    <button class="btn btn-primary" id="btnDealerAdd" onclick="createDealer()">Tạo Đại Lý</button>
    <button class="btn btn-light" data-bs-dismiss="modal">Close</button>
    `);

    $("#modalDealerBody").html(`
        <div class=" row mb-4">
        <label class="col-md-4 form-label" for="example-email">Email Đại Lý</label>
            <div class="col-md-8">
                <input type="email" id="email" class="form-control" placeholder="Email Đại Lý">
            </div>
        </div>
        <div class=" row mb-4">
        <label class="col-md-4 form-label">Password</label>
            <div class="col-md-8">
                <input type="password" id="password" class="form-control" placeholder="Nhập Password"> 
            </div>
        </div>
        <div class=" row mb-4">
        <label class="col-md-4 form-label">Confirm Password</label>
            <div class="col-md-8"> 
                <input type="password" id="repassword" class="form-control" placeholder="Nhập Lại Password"> 
            </div>
        </div>
        <div class="mb-0 row justify-content-end">
            <div class="col-md-8"> 
                <label class="custom-control custom-checkbox"> 
                <input type="checkbox" class="custom-control-input" id="requireverify">
                <span class="custom-control-label">Yêu Cầu Nhập Mã Xác Minh Email</span>
                </label> 
            </div>
        </div>
        <div class="mb-0 row justify-content-end">
            <div class="col-md-8"> 
                <label class="custom-control custom-checkbox"> 
                <input type="checkbox" class="custom-control-input" id="requireactive">
                <span class="custom-control-label">Cần Xét Duyệt Truy Cập Bởi Admin</span>
                </label> 
            </div>
        </div>
        `);

    $('#modalDealer').modal('show');
};


const createDealer = async() => {
    if (!$("#email").val() ||
        !$("#password").val() ||
        !$("#repassword").val()) {
        Notiflix.Notify.Failure('Vui lòng điền đầy đủ thông tin!');
        return;
    }

    if ($("#email").val().length <= 6) {
        Notiflix.Notify.Failure('Email không hợp lệ!');
        return;
    }

    if ($("#password").val().length <= 6) {
        Notiflix.Notify.Failure('Mật khẩu không được nhỏ hơn 6 kí tự!');
        return;
    }
    if ($("#password").val() != $("#repassword").val()) {
        Notiflix.Notify.Failure('2 Mật khẩu không khớp!');
        return;
    }



    var settings = {
        "url": localStorage.getItem('api') + "/api/auth/dealer-register",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "requireactive": $("#requireactive").prop("checked"),
            "requireverify": $("#requireverify").prop("checked"),
            "email": $("#email").val(),
            "password": $("#password").val()
        }),
    };

    $("#btnDealerAdd").prop("disabled", true);

    try {
        await $.ajax(settings).done((data) => {
            const resp = data.data;
            Notiflix.Report.Success(
                'Tạo đại lý thành công',
                'Hệ thống sẽ tải lại sau vài giây...',
                'Đồng ý'
            );
            setTimeout(() => {
                window.location.reload();
            }, 2000);
        }).fail((data) => {
            $("#btnDealerAdd").prop("disabled", false);
            if (data.responseJSON.error == "This email has been sent to another user") {
                Notiflix.Report.Failure(
                    'Tạo đại lý thất bại',
                    'Email này đã được sử dụng cho một tài khoản khác!',
                    'Thử lại'
                );
            } else {
                Notiflix.Report.Failure(
                    'Tạo đại lý thất bại',
                    data.error,
                    'Thử lại'
                );
            }
        });
    } catch (e) {
        console.log(e.message);
        $("#btnDealerAdd").prop("disabled", false);
    }
};