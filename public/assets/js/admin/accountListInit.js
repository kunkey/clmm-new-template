$(document).ready(() => {
    $.ajax({
        "url": localStorage.getItem('api') + `/api/admin/account`,
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {
        var i = 1;
        var taskStatus = false;
        for (var data of resp) {
            var body = "<tr>";
            body += `<td>${i}</td>`;
            body += `<td><b>${data.username}</b></td>`;
            body += `<td><b>${data.uid}</b></td>`;
            body += `<td><b>${data.email}</b></td>`;
            var accountStatus = (data.isLive == "true") ? `<button class="btn btn-sm btn-outline-success">LIVE</button>` : `<button class="btn btn-sm btn-outline-danger">DEAD</button>`;
            body += `<td>${accountStatus}</td>`;
            body += `<td>$<b>${data.realBalance}</b></td>`;
            var betStatus = (data.betStatus == "true") ? `<button class="btn btn-sm btn-outline-success">Bật</button>` : `<button class="btn btn-sm btn-outline-danger">Tắt</button>`;
            body += `<td>${betStatus}</td>`;
            body += `<td>${moment(data.createdAt).format("DD/MM/YYYY HH:mm")}</td>`;
            body += `<td><button  title="Xóa" onclick="modalDelete(${data.id})" type="button" class="btn btn-icon  btn-danger"><i class="fe fe-trash"></i></button></td>`;
            body += "</tr>";
            $("#accountListBody").append(body);
            i++;
        }
        taskStatus = true;
        if (taskStatus) {
            $('#accountList').DataTable({
                language: {
                    searchPlaceholder: 'Tìm Kiếm...',
                    sSearch: '',
                }
            });
        }
    }).fail((resp) => {
        killSession();
    });
});


const modalDelete = (id) => {
    Notiflix.Confirm.Show(
        'Bạn có muốn xóa tài khoản này không?',
        'Tất cả dữ liệu của tài khoản sẽ bị loại bỏ hệ thống?',
        'Đồng ý',
        'Hủy bỏ',
        (e) => {
            $.ajax({
                "url": localStorage.getItem('api') + `/api/admin/account/delete`,
                "method": "POST",
                "dataType": "json",
                "headers": {
                    "Access-Control-Allow-Origin": "*",
                    "Content-type": "application/json",
                    "Authorization": `Bearer ${access_token}`
                },
                "data": JSON.stringify({
                    "id": id
                }),
            }).done((resp) => {
                window.location.reload();
            }).fail((resp) => {
                window.location.reload();
            });
        });
};