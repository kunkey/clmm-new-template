$(document).ready(() => {
    $.ajax({
        "url": localStorage.getItem('api') + `/api/admin/user`,
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {
        var i = 1;
        var taskStatus = false;
        for (var data of resp) {
            var user = data.user,
                dealer = data.dealer,
                account = data.account;

            var body = "<tr>";
            body += `<td>${i}</td>`;
            body += `<td><b>${user.username}</b></td>`;
            body += `<td><b>${user.email}</b></td>`;
            body += `<td><b>${dealer.username}</b></td>`;
            body += `<td><b>${(account !== null) ? (account.email !== null) ? account.email: "Chưa thêm" : "Chưa thêm"}</b></td>`;
            var userStatus = (user.status == "blocked") ? `<button class="btn btn-sm btn-outline-danger">Đã bị khóa</button>` : (user.status == "active") ? `<button class="btn btn-sm btn-outline-success">Hoạt Động</button>` : `<button class="btn btn-sm btn-outline-danger">Không Hoạt Động</button>`;
            body += `<td>${userStatus}</td>`;
            var userVerify = (user.verify == "true") ? `<button class="btn btn-sm btn-outline-success">Đã xác minh</button>` : `<button class="btn btn-sm btn-outline-danger">Chưa xác minh</button>`;
            body += `<td><b>${userVerify}</b></td>`;
            body += `<td>${moment(user.createdAt).format("DD/MM/YYYY HH:mm")}</td>`;
            body += `<td>`;
            if (user.status == "pending") {
                body += `<button onclick="allowAccess(${user.id})"  style="margin-right:3px;" title="Duyệt" id="allowAccess" type="button" class="btn btn-icon  btn-info"><i class="fe fe-shield"></i></button>`;
            }
            body += `<button  title="Xóa" onclick="modalDelete(${user.id})"  style="margin-right:3px;" type="button" class="btn btn-icon  btn-danger"><i class="fe fe-trash"></i></button></td>`;
            body += "</tr>";
            $("#userListBody").append(body);
            i++;
        }
        taskStatus = true;
        if (taskStatus) {
            $('#userList').DataTable({
                language: {
                    searchPlaceholder: 'Tìm Kiếm...',
                    sSearch: '',
                }
            });
        }
    }).fail((resp) => {
        killSession();
    });
});


const allowAccess = (id) => {
    $.ajax({
        "url": localStorage.getItem('api') + `/api/admin/user/update-status`,
        "method": "POST",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        "data": JSON.stringify({
            "id": id,
            "status": "active"
        }),
    }).done((resp) => {
        window.location.reload();
    }).fail((resp) => {
        window.location.reload();
    });
}



const modalDelete = (id) => {
    Notiflix.Confirm.Show(
        'Bạn có muốn xóa tài khoản này không?',
        'Tất cả dữ liệu của tài khoản sẽ bị loại bỏ hệ thống?',
        'Đồng ý',
        'Hủy bỏ',
        (e) => {
            $.ajax({
                "url": localStorage.getItem('api') + `/api/admin/user/delete`,
                "method": "POST",
                "dataType": "json",
                "headers": {
                    "Access-Control-Allow-Origin": "*",
                    "Content-type": "application/json",
                    "Authorization": `Bearer ${access_token}`
                },
                "data": JSON.stringify({
                    "id": id
                }),
            }).done((resp) => {
                window.location.reload();
            }).fail((resp) => {
                window.location.reload();
            });
        });
};