$(document).ready(() => {
    $.ajax({
        "url": localStorage.getItem('api') + `/api/admin/dashboard`,
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {
        if (resp.status) {
            const data = resp.data;
            $("#userCount").html(data.totalUser);
            $("#userPendingCount").html(data.totalUserPending);
            $("#packageOrderCount").html(data.totalOrderPending);
            $("#packageMoney").html(`${data.totalMoneyOrder}$`);
        }
    }).fail((resp) => {
        killSession();
    });
});


var gaugeChart = AmCharts.makeChart("chartdiv", {
    "type": "gauge",
    "faceAlpha": 1,
    "faceColor": "#363636",
    "color": "#FAFAFA",
    "fontSize": 15,
    "arrows": [{
        "color": "#696969",
        "id": "GaugeArrow-1",
        "innerRadius": 0,
        "nailBorderThickness": 4,
        "nailRadius": 22,
        "startWidth": 22,
        "value": 0
    }],
    "axes": [{
        "axisThickness": 0,
        "bottomText": "0 %",
        "bottomTextYOffset": 20,
        "endValue": 100,
        "id": "GaugeAxis-1",
        "labelOffset": 7,
        "minorTickLength": 0,
        "tickColor": "#FAFAFA",
        "tickLength": 15,
        "tickThickness": 2,
        "valueInterval": 10,
        "bands": [{
                "color": "#32CD32",
                "endValue": 3.8,
                "id": "GaugeBand-1",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 0
            },
            {
                "color": "#15FF00",
                "endValue": 7.8,
                "id": "GaugeBand-2",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 4
            },
            {
                "color": "#2AFF00",
                "endValue": 11.8,
                "id": "GaugeBand-3",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 8
            },
            {
                "color": "#40FF00",
                "endValue": 15.8,
                "id": "GaugeBand-4",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 12
            },
            {
                "color": "#55FF00",
                "endValue": 19.8,
                "id": "GaugeBand-5",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 16
            },
            {
                "color": "#6AFF00",
                "endValue": 23.8,
                "id": "GaugeBand-6",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 20
            },
            {
                "color": "#7FFF00",
                "endValue": 27.8,
                "id": "GaugeBand-7",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 24
            },
            {
                "color": "#95FF00",
                "endValue": 31.8,
                "id": "GaugeBand-8",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 28
            },
            {
                "color": "#AF0",
                "endValue": 35.8,
                "id": "GaugeBand-9",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 32
            },
            {
                "color": "#BFFF00",
                "endValue": 39.8,
                "id": "GaugeBand-10",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 36
            },
            {
                "color": "#D4FF00",
                "endValue": 43.8,
                "id": "GaugeBand-11",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 40
            },
            {
                "color": "#EAFF00",
                "endValue": 47.8,
                "id": "GaugeBand-12",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 44
            },
            {
                "alpha": 1,
                "color": "#FF0",
                "endValue": 51.8,
                "id": "GaugeBand-13",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 48
            },
            {
                "color": "#FF0",
                "endValue": 55.8,
                "id": "GaugeBand-14",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 52
            },
            {
                "color": "#FFEA00",
                "endValue": 59.8,
                "id": "GaugeBand-15",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 56
            },
            {
                "color": "#FFD500",
                "endValue": 63.8,
                "id": "GaugeBand-16",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 60
            },
            {
                "color": "#FA0",
                "endValue": 67.8,
                "id": "GaugeBand-17",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 64
            },
            {
                "color": "#FF9500",
                "endValue": 71.8,
                "id": "GaugeBand-18",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 68
            },
            {
                "color": "#FF7F00",
                "endValue": 75.8,
                "id": "GaugeBand-19",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 72
            },
            {
                "color": "#FF6A00",
                "endValue": 79.8,
                "id": "GaugeBand-20",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 76
            },
            {
                "color": "#F50",
                "endValue": 83.8,
                "id": "GaugeBand-21",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 80
            },
            {
                "color": "#FF4000",
                "endValue": 87.8,
                "id": "GaugeBand-22",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 84
            },
            {
                "color": "#FF2B00",
                "endValue": 91.8,
                "id": "GaugeBand-23",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 88
            },
            {
                "color": "#FF1500",
                "endValue": 95.8,
                "id": "GaugeBand-24",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 92
            },
            {
                "color": "#FF0000",
                "endValue": 99.8,
                "id": "GaugeBand-25",
                "innerRadius": "60%",
                "radius": "75%",
                "startValue": 96
            }
        ]
    }],
    "allLabels": [],
    "balloon": {},
});

setInterval(randomValue, 3000);

// set random value
function randomValue() {
    if (gaugeChart) {
        if (gaugeChart.arrows) {
            if (gaugeChart.arrows[0]) {
                if (gaugeChart.arrows[0].setValue) {
                    $.ajax({
                        "url": localStorage.getItem('api') + `/api/admin/dashboard/stats`,
                        "method": "GET",
                        "dataType": "json",
                        "headers": {
                            "Access-Control-Allow-Origin": "*",
                            "Content-type": "application/json",
                            "Authorization": `Bearer ${access_token}`
                        },
                    }).done((resp) => {
                        if (resp.status) {
                            const data = resp.data;
                            gaugeChart.arrows[0].setValue(parseInt(data.cpu.cpu, 10));
                            gaugeChart.axes[0].setBottomText(parseInt(data.cpu.cpu, 10) + " %");
                        }
                    }).fail((resp) => {});
                }
            }
        }
    }
}