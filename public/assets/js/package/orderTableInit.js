$(document).ready(() => {
    $.ajax({
        "url": localStorage.getItem('api') + "/api/package/orders",
        "method": "GET",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
    }).done((resp) => {
        if (resp.status) {
            var i = 1;
            var taskStatus = false;
            for (var data of resp.data) {
                var body = "<tr>";
                body += `<td>${i}</td>`;
                body += `<td><b>#${data.OrderId}</b></td>`;
                body += `<td><b>${data.packageText}</b></td>`;
                body += `<td><b>$${data.price}</b></td>`;
                var status;
                if (data.status == "pending") {
                    status = `<button class="btn btn-sm btn-outline-warning">Chờ duyệt</button>`
                } else if (data.status == "success") {
                    status = `<button class="btn btn-sm btn-outline-success">Thành công</button>`
                } else if (data.status == "error") {
                    status = `<button class="btn btn-sm btn-outline-danger">Lỗi</button>`;
                }
                body += `<td>${status}</td>`;
                body += `<td>${moment(data.createdAt).format("DD/MM/YYYY HH:MM")}</td>`;
                body += "</tr>";
                $("#packageOrdersBody").append(body);
                i++;
            }
            taskStatus = true;
            if (taskStatus) {
                $('#packageOrders').DataTable({
                    language: {
                        searchPlaceholder: 'Tìm Kiếm...',
                        sSearch: '',
                    }
                });
            }

        }
    }).fail((resp) => {
        killsession();
    });
});