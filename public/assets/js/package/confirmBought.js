$(document).ready(() => {
    $("#xacnhan").on("click", (e) => {
        Notiflix.Confirm.Show(
            'Vui lòng xác nhận chuyển khoản?',
            'Bạn đã xác nhận rằng bạn đã thực hiện đúng theo các yêu cầu?',
            'Xác nhận',
            'Hủy bỏ',
            (e) => {
                confirmTrue();
            });
    });
});


const confirmTrue = () => {
    $.ajax({
        "url": localStorage.getItem('api') + "/api/package/",
        "method": "POST",
        "dataType": "json",
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        "data": JSON.stringify({
            "package": $("#pakageName").val()
        }),
    }).done(async(resp) => {
        if (resp.status) {
            Notiflix.Report.Success(
                'Thành Công',
                'Gửi yêu cầu mua gói thành công, yêu cầu của bạn sẽ được thực hiện trong ít phút!',
                'Hoàn Tất'
            );
        } else {
            Notiflix.Notify.Failure(resp.message);
        }
    }).fail((resp) => {
        killsession();
    });
}