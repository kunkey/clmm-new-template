const getRefererModal = () => {
    $("#modalTitleDealer").html('Giới Thiệu người dùng!');
    $("#modalFooterDealer").html('<button class="btn btn-light" data-bs-dismiss="modal">Đóng</button>');
    $("#modalBodyDealer").html(`
        <p><i>* Vui lòng copy link giới thiệu của bạn trong khung!</i></p>
        <div class=" row mb-4"> <label class="col-md-4 form-label" for="example-email">LINK GIỚI THIỆU</label>
        <div class="col-md-8">
            <div class="input-group"> 
                <input type="text"  id="inputIntro" class="form-control" placeholder="Link giới thiệu của bạn" value="https://rioluxury.net/register?ref=${localStorage.getItem('id')}"> 
                <span class="input-group-text btn btn-primary" onclick="copyIntroLink()">Copy</span>
            </div>
        </div>
    </div>
        `);
    $("#modalDealer").modal('show');
}


const copyIntroLink = () => {
    var copyText = document.querySelector("#inputIntro");
    copyText.select();
    document.execCommand("copy");
    Notiflix.Report.Success(
        'Đã sao chép!',
        'Chia sẻ link giới thiệu này cho bạn bè của bạn!',
        'Đóng'
    );
}