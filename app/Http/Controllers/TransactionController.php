<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AccountMomo;
use App\Models\LichSuChoiMomo;

class TransactionController extends Controller
{
    public function Get_Transaction(request $request){
        $LichSuChoiMomo = new LichSuChoiMomo();
        $GetLichSuChoiMomo = $LichSuChoiMomo->where(
            'magiaodich', $_POST["id"]
        )->get();
            
        if(!$GetLichSuChoiMomo->count()) {
            return response()->json([
                'status' => false,
                'message' => "Không tìm thấy giao dịch cho mã giao dịch bạn vừa nhập!"
            ]);
        }else {
            return response()->json([
                'status' => true,
                'data' => $GetLichSuChoiMomo[0],
                'message' => "success!"
            ]);    
        }
    }
}