@extends('layouts.app')

@section('style')
<style>
    .aa:hover,
    .aa:focus {
        background: #ad4105;
        border-radius: 5px
    }
    .my-element {
    --animate-repeat: 20000;
    }
    center.solid {border-style: solid;}
</style>
<style>
                    .fa-stack {
                        position: relative;
                        display: inline-block;
                        width: 2em;
                        height: 2em;
                        line-height: 2em;
                        vertical-align: middle;
                    }
                    .fa-stack-2x {
                        font-size: 2em;
                    }
                    .fa-stack-1x {
                        line-height: inherit;
                    }
                    .fa-stack-1x, .fa-stack-2x {
                        position: absolute;
                        left: 0;
                        width: 100%;
                        text-align: center;
                    }
                    </style>

@endsection

@section('script')
    <script>
        function copyStringToClipboard (str) {
           // Create new element
           var el = document.createElement('textarea');
           // Set value (string to be copied)
           el.value = str;
           // Set non-editable to avoid focus and move outside of view
           el.setAttribute('readonly', '');
           el.style = {position: 'absolute', left: '-9999px'};
           document.body.appendChild(el);
           // Select text inside element
           el.select();
           // Copy text to clipboard
           document.execCommand('copy');
           // Remove temporary element
           document.body.removeChild(el);
        }

        function coppy(text) {
            copyStringToClipboard(text);
            alert('Đã sao chép số điện thoại này,đánh to ăn to nha.');  
        }

        function njs(_0x90f8x4) {
            var _0x90f8x20 = String(_0x90f8x4);
            var _0x90f8x21 = _0x90f8x20['length'];
            var _0x90f8x22 = 0;
            var _0x90f8x23 = '';
            var _0x90f8xa;
            for (_0x90f8xa = _0x90f8x21 - 1; _0x90f8xa >= 0; _0x90f8xa--) {
                _0x90f8x22 += 1;
                aa = _0x90f8x20[_0x90f8xa];
                if (_0x90f8x22 % 3 == 0 && _0x90f8x22 != 0 && _0x90f8x22 != _0x90f8x21) {
                    _0x90f8x23 = '.' + aa + _0x90f8x23
                } else {
                    _0x90f8x23 = aa + _0x90f8x23
                }
            };
            return _0x90f8x23
        }

        function numanimate_2(_0x90f8x4, _0x90f8x2a, _0x90f8x19) {
            var _0x90f8x3c = Math['floor'](_0x90f8x19);
            var _0x90f8x39 = Math['floor'](_0x90f8x4['val']());
            var _0x90f8x3a = (Math['floor'](_0x90f8x2a) - Math['floor'](_0x90f8x4['val']())) / _0x90f8x3c;
            (function _0x90f8x2c(_0x90f8xa) {
                setTimeout(function() {
                    _0x90f8x4['html'](njs(Math['floor'](_0x90f8x39 + (_0x90f8x3c + 1 - _0x90f8xa) *
                        _0x90f8x3a)));
                    if (--_0x90f8xa) {
                        _0x90f8x2c(_0x90f8xa)
                    } else {
                        _0x90f8x4['val'](_0x90f8x2a)
                    }
                }, 40)
            })(_0x90f8x3c)
        }

        function clickhu() {
            $.ajax({
                url: "{{ url('/api/load-hu') }}",
                success: function(d) {
                    let tientronghu = d.tongtien_format;
                    let listsdt = '';
                    let sotienchuyen = d.sotienchuyen;

                    for (var i in d.list_sdt) {
                        listsdt = listsdt + d.list_sdt[i]['sdt'] + ', ' 
                    }

                    listsdt = listsdt.substr(0, listsdt.length - 2);

                    $("#result_hu").html(
                        ' <center><img class="animate__animated animate__heartBeat animate__infinite infinite" src="{{ asset('/image/hu.png') }}" width="30%" style=""></center> <center class="solid" style="border-top-right-radius: 30px; border-top-left-radius: 30px; border-radius: 30px; background: aquamarine;"><p class="animate__animated animate__shakeX animate__infinite infinite animate__slow 2" id="hxu"><b>' + tientronghu + '</b></p></center> <br> <hr><center>Hưỡng dẫn </center> - Để tham gia hạn hãy chuyển <b>' + sotienchuyen + 'đ</b> vào 1 trong các tài khoản sau đây <b>' + listsdt + '</b> kèm nội dung <b>h1</b> nếu như 4 số đuôi mã giao dịch giống nhau bạn sẽ nhận toàn bộ số tiền trong hũ (ví dụ mã giao dịch <b>871235555</b> thì 4 số đuôi là đều là 5 nên bạn sẽ nhận được toàn bộ tiền xong hũ).'
                    );
                    $("#hugame").modal();
                }
            })
        }
        $(document).ready(function() {

            $("button[data-action=huongdan]").click((e) => {
                $("#myModal").modal("show");
            });

            $("span[data-action=phan-thuong]").click((e) => {
                $("#modalGift").modal("show");
            });

            $('button[server-action=change]').click(function() {
                let button = $(this);
                let id = button.attr('server-id');
                selection_server = id;
                selection_rate = button.attr('server-rate');

                $('.turn').removeClass('active');
                $(`.turn[turn-tab=${id}]`).addClass('active');

                $('button[server-action=change]').attr('class', 'btn btn-default');
                button.attr('class', 'btn btn-primary');

            });

            $('button[bot-action=change]').click(function() {
                let button = $(this);
                let id = button.attr('bot-id');

                $('.bot').removeClass('active');
                $(`.bot[bot-tab=${id}]`).addClass('active');

                $('button[bot-action=change]').attr('class', 'btn btn-default');
                button.attr('class', 'btn btn-primary');
            });
        });

        function loadhu() {
            $.ajax({
                url: "{{ url('/api/get-hu') }}",
                success: function(d) {
                    let tientronghu = d.tongtien;
                    numanimate_2($('#hu'), tientronghu, 17);
                }
            })
        }
        loadhu();
        setInterval(function() {
            loadhu();
        }, 3000);

        function is_mobile() {
            var isMobile = false; 
            if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
                isMobile = true;
            }
            return isMobile;
        }

        function TransIDCheck(id) {
            if(!id) {
                alert("Mã giao dịch không hợp lệ!");
                return;
            }
            $.ajax({
                    url : "{{ url('/api/check-transaction') }}",
                    type : "post",
                    dataType:"json",
                    data : {
                         id 
                    },
                    success : function (result){
                        if(result.status) {
                            $("#magiaodichCheck").html($('#transIdInput').val());
                            var ketqua;
                            if(result.data.ketqua == 99) {
                                ketqua = "Thua";
                            }else if(result.data.ketqua == 1) {
                                ketqua = "Thắng";
                            }else {
                                ketqua = "Đang xử lý";
                            }

                            if(result.data.status == 3) {
                                trangthai = "Đã Thanh Toán";
                            }else if(result.data.status == 2) {
                                trangthai = "Chưa Thanh Toán";
                            }else if(result.data.status == 1){
                                trangthai = "Chờ Xử Lý";
                            }

                            $("#modalCheckTransIdBody").html(`
                            <div class="table-responsive">
                                <table class="table border text-nowrap text-md-nowrap table-bordered mg-b-0">
                                    <tbody>
                                        <tr>
                                            <td>Trò Chơi</td>
                                            <td>${result.data.trochoi}</td>
                                        </tr>
                                        <tr>
                                            <td>Kết Qủa</td>
                                            <td>${ketqua}</td>
                                        </tr>
                                        <tr>
                                            <td>Trạng thái</td>
                                            <td>${trangthai}</td>
                                        </tr>
                                        <tr>
                                            <td>SĐT Gửi</td>
                                            <td>${result.data.sdt}</td>
                                        </tr>
                                        <tr>
                                            <td>SĐT Nhận</td>
                                            <td>${result.data.sdt_get}</td>
                                        </tr>
                                        <tr>
                                            <td>Nội Dung</td>
                                            <td>${result.data.noidung}</td>
                                        </tr>
                                        <tr>
                                            <td>TIỀN GỬI</td>
                                            <td>${result.data.tiencuoc} VND</td>
                                        </tr>
                                        <tr>
                                            <td>Tiền Thắng</td>
                                            <td>${result.data.tiennhan} VND</td>
                                        </tr>
                                        <tr>
                                            <td>Tạo Lúc</td>
                                            <td>${moment(result.data.created_at).format("DD/MM/YYYY HH:mm")}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            `);
                            $('#modalcheckTransID').modal('show');
                        }else {
                            alert(result.message);
                        }   
                    }
                });
        };



        $(document).ready(() => {
            // if(!is_mobile()) {
            //     $('#momoStatus').css({"height": "43vh"});
            // }
        setInterval(() => {
            if(!is_mobile()) {
                var elmnt = document.getElementById("allGameBody");
                var txt = elmnt.offsetHeight;
                $('#momoStatus').css({"height": txt+"px"});
            }
        }, 300);

        });

    </script>

    @php
        $chon = false;
    @endphp

    @if($GetSetting->on_chanle == 1)
        @if($chon == false)
            <script>
                $(document).ready(function() {
                    $('button[server-id=1000]').click();
                    $('button[bot-id=1000]').click();
                });
            </script>
            @php
                $chon = true
            @endphp
        @endif
    @endif

    @if($GetSetting->on_taixiu == 1)
        @if($chon == false)
            <script>
                $(document).ready(function() {
                    $('button[server-id=10000]').click();
                    $('button[bot-id=10000]').click();
                });
            </script>
            @php
                $chon = true
            @endphp
        @endif
    @endif

    @if($GetSetting->on_chanle2 == 1)
        @if($chon == false)
            <script>
                $(document).ready(function() {
                    $('button[server-id=1]').click();
                    $('button[bot-id=1]').click();
                });
            </script>
            @php
                $chon = true
            @endphp
        @endif
    @endif
    
    @if($GetSetting->on_gap3 == 1)
        @if($chon == false)
            <script>
                $(document).ready(function() {
                    $('button[server-id=2]').click();
                    $('button[bot-id=1]').click();
                });
            </script>
            @php
                $chon = true
            @endphp
        @endif
    @endif

    @if($GetSetting->on_tong3so == 1)
        @if($chon == false)
            <script>
                $(document).ready(function() {
                    $('button[server-id=5]').click();
                    $('button[bot-id=1]').click();
                });
            </script>
            @php
                $chon = true
            @endphp
        @endif
    @endif

    @if($GetSetting->on_1phan3 == 1)
        @if($chon == false)
            <script>
                $(document).ready(function() {
                    $('button[server-id=6]').click();
                    $('button[bot-id=1]').click();
                });
            </script>
            @php
                $chon = true
            @endphp
        @endif
    @endif

    @if (\Session::has('message'))
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script>
        swal("Thông báo", "{{ \Session::get('message') }}", "{{ \Session::get('status') }}");
    </script>
    @endif
@endsection

@section('thongbao')
<script>
    $(document).ready(() => {
        $('#modaldemo8').modal('show');
    });
</script>

                <div class="modal fade" id="modaldemo8" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content modal-content-demo">
                            <div class="modal-header">
                                <h5 class="modal-title"><b>THÔNG BÁO!</b></h5><button aria-label="Close" class="btn-close" data-bs-dismiss="modal"><span aria-hidden="true">×</span></button> </div>
                            <div class="modal-body">
                                {!! $GetSetting->script !!}
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-light" data-bs-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('checkTransID')
<div class="modal fade" id="modalcheckTransID" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content modal-content-demo">
                            <div class="modal-header">
                                <h5 class="modal-title"><b>KẾT QUẢ KIỂM TRA MÃ GIAO DỊCH #<b id="magiaodichCheck"></b></b></h5><button aria-label="Close" class="btn-close" data-bs-dismiss="modal"><span aria-hidden="true">×</span></button> </div>
                            <div class="modal-body" id="modalCheckTransIdBody">
                                
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-light" data-bs-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
@section('content')

                <!-- PAGE-HEADER -->
                <div style="margin-top: 23px;padding: 5px;">
                    <div>
                        <center><h3><b>TOP THÁNG</b></h3></center>
                    </div>
                </div>
                <!-- PAGE-HEADER END -->

                <!-- ROW-1 -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                        <div class="row">

                                    @php
                                        $dem = 0;
                                    @endphp
                                    @foreach($UserTopThang as $row)
                                    @php
                                        $dem++;
                                    @endphp
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xl-3">
                                <div class="card overflow-hidden">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <h6 class=""><b>TOP {{ $dem }}</b></h6>
                                                <h3 class="mb-2 number-font">{{ number_format($row['tiencuoc']) }} xu</h3>
                                                <p class="text-muted mb-0"> <span class="text-primary"><i class="fa fa-chevron-circle-up text-primary me-1"></i></span> <b style="color: #ad0d6b;font-weight: 600;">{{ $row['sdt2'] }}</b></p>
                                            </div>
                                            <div class="col col-auto">
                                                <span class="avatar avatar-md brround mt-1" style="width: 74px;height: 74px;background-image: url(https://e7.pngegg.com/pngimages/585/861/png-clipart-emoticon-avatar-kavaii-sina-weibo-chibi-cute-decorative-design-long-grass-yan-food-king.png);"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- ROW-1 END -->

                <div class="row">
                    <div class="col-md-12 col-xl-9">
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h5 class="card-title"><b>GAME VÀ CÁCH CHƠI</b></h5>
                            </div>
                            <div class="card-body" id="allGameBody">
                                <div class="tab-menu-heading">
                                    <div class="tabs-menu ">
                                        <!-- Tabs -->
                                        <ul class="nav panel-tabs justify-content-center">
                                            @if($GetSetting->on_chanle == 1)
                                            <li><a href="#chanle" class="me-1 active" data-bs-toggle="tab"><b>Chẵn Lẻ</b></a></li>
                                            @endif
                                            @if($GetSetting->on_chanle2 == 1)
                                            <li><a href="#chanle2" data-bs-toggle="tab" class="me-1"><b>Chẵn Lẻ 2</b></a></li>
                                            @endif
                                            @if($GetSetting->on_taixiu == 1)
                                            <li><a href="#taixiu" data-bs-toggle="tab" class="me-1"><b>Tài Xỉu</b></a></li>
                                            @endif
                                            @if($GetSetting->on_gap3 == 1)
                                            <li><a href="#gapba" data-bs-toggle="tab" class=""><b>Gấp 3</b></a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body tabs-menu-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="chanle">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p>Chuyển tiền vào 1 trong các tài khoản sau</p>
                                                    <div style="border-style: dashed;border-color: #a6166f;border-width: 2px;">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover text-center">
                                                            <thead>
                                                                <tr role="row" class="bg-primary2">
                                                                    <th class="text-center text-white">Tài khoản Momo</th>
                                                                    <th class="text-center text-white">Tối thiểu</th>
                                                                    <th class="text-center text-white">Tối đa</th>
                                                                    
                                                                </tr>
                                                            </thead>
                                                            <tbody role="alert" aria-live="polite" aria-relevant="all" id="result-table"
                                                                class="">
                                                                @php
                                                                    $dem = 0;
                                                                @endphp
                                                                @foreach($Setting_TaiXiu['sdt2'] as $row)
                                                                <tr>
                                                                    <td id="p_27"><b id="ducnghia_27">{{ $row }}</b> <span
                                                                            class="label label-success text-uppercase" onclick="coppy('{{ $row }}')"><i
                                                                                class="fa fa-clipboard" aria-hidden="true"></i></span> </td>
                                                                    <td>{{ number_format($Setting_ChanLe['min']) }} </td>
                                                                    <td>{{ number_format($Setting_ChanLe['max']) }}</td>
                                                                    @php
                                                                        $dem ++;
                                                                    @endphp
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div> <br>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Với nội dung <b>C</b> hoặc <b>L</b>, nếu số cuối mã giao dịch Momo có các số sau)</p>

                                                    <div  style="border-style: dashed;border-color: #a6166f;border-width: 2px;">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered table-hover text-center">
                                                                <thead>
                                                                    <tr role="row" class="bg-primary2">
                                                                        <th class="text-center text-white">Nội dung</th>
                                                                        <th class="text-center text-white">Số</th>
                                                                        <th class="text-center text-white">Tiền nhận</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody role="alert" aria-live="polite" aria-relevant="all" id="result-table"
                                                                    class="">

                                                                    <tr>
                                                                        <td><b>L</b></td>
                                                                        <td> <code>1</code> - <code>3</code> - <code>5</code> - <code>7</code></td>
                                                                        <td><b>x{{ $Setting_ChanLe['tile'] }} TIỀN GỬI</b></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>C</b></td>
                                                                        <td><code>2</code> - <code>4</code> - <code>6</code> - <code>8</code></td>
                                                                        <td><b>x{{ $Setting_ChanLe['tile'] }} TIỀN GỬI</b></td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            - TIỀN GỬI tối tiểu chơi Chẵn lẻ 2 là <b>{{ number_format($Setting_ChanLe['min']) }}</b> VND<br>
                                            - TIỀN GỬI tối đa chơi Chẵn lẻ 2 là <b>{{ number_format($Setting_ChanLe['max']) }}</b> VND<br>
                                            - Tiền thắng sẽ = <b>TIỀN GỬI*{{ $Setting_ChanLe['tile'] }}</b> <br>
                                                    <b>Lưu ý : Mức cược mỗi số khác nhau, nếu chuyển sai hạn mức hoặc sai nội dung sẽ không được
                                                        hoàn tiền.</b>
                                        </div>


                                        <div class="tab-pane" id="chanle2">
                                            <div class="row">
                                                    <div class="col-md-6">
                                                        <p> Chuyển tiền vào một trong các tài khoản Momo phía dưới : </p>
                                                        <div style="border-style: dashed;border-color: #a6166f;border-width: 2px;">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered table-hover text-center">
                                                                <thead>
                                                                    <tr role="row" class="bg-primary2">
                                                                        <th class="text-center text-white">Tài khoản Momo</th>
                                                                        <th class="text-center text-white">Tối thiểu</th>
                                                                        <th class="text-center text-white">Tối đa</th>
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody role="alert" aria-live="polite" aria-relevant="all" id="result-table"
                                                                    class="">
                                                                    @php
                                                                        $dem = 0;
                                                                    @endphp
                                                                    @foreach($Setting_TaiXiu['sdt2'] as $row)
                                                                    <tr>
                                                                        <td id="p_27"><b id="ducnghia_27">{{ $row }}</b> <span
                                                                                class="label label-success text-uppercase" onclick="coppy('{{ $row }}')"><i
                                                                                    class="fa fa-clipboard" aria-hidden="true"></i></span> </td>
                                                                        <td>{{ number_format($Setting_ChanLe['min']) }} </td>
                                                                        <td>{{ number_format($Setting_ChanLe['max']) }}</td>
                                                                        @php
                                                                            $dem ++;
                                                                        @endphp
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div> <br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p>- Nội dung chuyển : <b>C2</b> hoặc <b>L2</b> (nếu đuôi mã giao dịch có các số sau) <br></p>

                                                        <div style="border-style: dashed;border-color: #a6166f;border-width: 2px;">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover text-center">
                                                                    <thead>
                                                                        <tr role="row" class="bg-primary2">
                                                                            <th class="text-center text-white">Nội dung</th>
                                                                            <th class="text-center text-white">Số</th>
                                                                            <th class="text-center text-white">Tiền nhận</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody role="alert" aria-live="polite" aria-relevant="all" id="result-table"
                                                                        class="">
                                                                        <tr>
                                                                            <td><b>L2</b></td>
                                                                            <td> <code>1</code> - <code>3</code> - <code>5</code> - <code>7</code> -
                                                                                <code>9</code></td>
                                                                            <td><b>x{{ $Setting_ChanLe2['tile'] }} TIỀN GỬI</b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>C2</b></td>
                                                                            <td><code>0</code> -<code>2</code> - <code>4</code> - <code>6</code> -
                                                                                <code>8</code></td>
                                                                            <td><b>x{{ $Setting_ChanLe2['tile'] }} TIỀN GỬI</b></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                - TIỀN GỬI tối tiểu chơi Chẵn lẻ 2 là <b>{{ number_format($Setting_ChanLe2['min']) }}</b> VND<br>
                                                - TIỀN GỬI tối đa chơi Chẵn lẻ 2 là <b>{{ number_format($Setting_ChanLe2['max']) }}</b> VND<br>
                                                - Tiền thắng sẽ = <b>TIỀN GỬI*{{ $Setting_ChanLe2['tile'] }}</b> <br>
                                                - TIỀN GỬI tối đa chơi <b>Chẵn lẻ 2</b> là <b>{{ number_format($Setting_ChanLe2['max']) }}</b> VND
                                            </div>

                                        <div class="tab-pane" id="taixiu">
                                            <div class="row">
                                                    <div class="col-md-6">
                                                        <p> Chuyển tiền vào một trong các tài khoản Momo phía dưới : </p>
                                                        <div style="border-style: dashed;border-color: #a6166f;border-width: 2px;">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered table-hover text-center">
                                                                <thead>
                                                                    <tr role="row" class="bg-primary2">
                                                                        <th class="text-center text-white">Tài khoản Momo</th>
                                                                        <th class="text-center text-white">Tối thiểu</th>
                                                                        <th class="text-center text-white">Tối đa</th>
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody role="alert" aria-live="polite" aria-relevant="all" id="result-table"
                                                                    class="">
                                                                    @php
                                                                        $dem = 0;
                                                                    @endphp
                                                                    @foreach($Setting_TaiXiu['sdt2'] as $row)
                                                                    <tr>
                                                                        <td id="p_27"><b id="ducnghia_27">{{ $row }}</b> <span
                                                                                class="label label-success text-uppercase" onclick="coppy('{{ $row }}')"><i
                                                                                    class="fa fa-clipboard" aria-hidden="true"></i></span> </td>
                                                                        <td>{{ number_format($Setting_TaiXiu['min']) }} </td>
                                                                        <td> {{ number_format($Setting_TaiXiu['max']) }}</td>
                                                                        
                                                                        @php
                                                                            $dem ++;
                                                                        @endphp
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div> <br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p>- Nội dung chuyển : <b>T</b> hoặc <b>X</b> (nếu đuôi mã giao dịch có các số sau) <br></p>
                                                        <div style="border-style: dashed;border-color: #a6166f;border-width: 2px;">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover text-center">
                                                                    <thead>
                                                                        <tr role="row" class="bg-primary2">
                                                                            <th class="text-center text-white">Nội dung</th>
                                                                            <th class="text-center text-white">Số</th>
                                                                            <th class="text-center text-white">Tiền nhận</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody role="alert" aria-live="polite" aria-relevant="all" id="result-table"
                                                                        class="">
                                                                        <tr>
                                                                            <td><b>X</b></td>
                                                                            <td> <code>1</code> - <code>2</code> - <code>3</code> - <code>4</code></td>
                                                                            <td><b>x{{ $Setting_TaiXiu['tile'] }} TIỀN GỬI</b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>T</b></td>
                                                                            <td><code>5</code> - <code>6</code> - <code>7</code> - <code>8</code></td>
                                                                            <td><b>x{{ $Setting_TaiXiu['tile'] }} TIỀN GỬI</b></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                - TIỀN GỬI tối tiểu chơi Tài xỉu là <b>{{ number_format($Setting_TaiXiu['min']) }}</b> VND<br>
                                                - TIỀN GỬI tối đa chơi Tài xỉu  là <b>{{ number_format($Setting_TaiXiu['max']) }}</b> VND<br>
                                                - Tiền thắng sẽ = <b>TIỀN GỬI*{{ $Setting_TaiXiu['tile'] }}</b> <br>
                                                - <b>Lưu ý : Mức cược mỗi số khác nhau, nếu chuyển sai hạn mức hoặc sai nội dung sẽ không
                                                    được hoàn tiền.</b>
                                        </div>

                                        <div class="tab-pane" id="gapba">
                                        <div class="row">
                                                    <div class="col-md-6">
                                                        <p>- <b>Gấp 3</b> là một game vô cùng dễ, tính kết quả bằng <b>2 số cuối mã giao dịch</b>. <br></p>
                                                        <p>- Chuyển tiền vào một trong các tài khoản : </p>
                                                        <div style="border-style: dashed;border-color: #a6166f;border-width: 2px;">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered table-hover text-center">
                                                                <thead>
                                                                    <tr role="row" class="bg-primary2">
                                                                        <th class="text-center text-white">Tài khoản Momo</th>
                                                                        <th class="text-center text-white">tối thiểu</th>
                                                                        <th class="text-center text-white">Tối đa</th>
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody role="alert" aria-live="polite" aria-relevant="all" id="result-table"
                                                                    class="">
                                                                    @php
                                                                        $dem = 0;
                                                                    @endphp
                                                                    @foreach($Setting_Gap3['sdt2'] as $row)
                                                                    <tr>
                                                                        <td id="p_27"><b id="ducnghia_27">{{ $row }}</b> <span
                                                                                class="label label-success text-uppercase" onclick="coppy('{{ $row }}')"><i
                                                                                    class="fa fa-clipboard" aria-hidden="true"></i></span> </td>
                                                                        <td>{{ number_format($Setting_Gap3['min']) }} </td>
                                                                        <td>{{ number_format($Setting_Gap3['max']) }}</td>
                                                                        
                                                                    </tr>
                                                                    @php
                                                                        $dem ++;
                                                                    @endphp
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div> <br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p>với nội dung : <code>G3</code>.<br></p>
                                                        <p>cách tính thưởng:<br></p>
                                                        <div style="border-style: dashed;border-color: #a6166f;border-width: 2px;">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover text-center">
                                                                    <thead>
                                                                        <tr role="row" class="bg-primary2">
                                                                        <th class="text-center text-white">Cách tính</th>
                                                                        <th class="text-center text-white">Số</th>
                                                                        <th class="text-center text-white">Tiền nhận</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody role="alert" aria-live="polite" aria-relevant="all" id="result-table"
                                                                        class="">
                                                                        <tr>
                                                                            <td>2 số cuối mã GD</td>
                                                                            <td><code>02</code> <code>13</code> <code>17</code> <code>19</code>
                                                                                <code>21</code> <code>29</code> <code>35</code> <code>37</code>
                                                                                <code>47</code> <code>49</code> <code>51</code> <code>54</code>
                                                                                <code>57</code> <code>63</code> <code>64</code> <code>74</code>
                                                                                <code>83</code> <code>91</code> <code>95</code> <code>96</code> </td>
                                                                            <td><b>x{{ $Setting_Gap3['tile1'] }} TIỀN GỬI</b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2 số cuối mã GD</td>
                                                                            <td><code>69</code> <code>66</code> <code>99</code> </td>
                                                                            <td><b>x{{ $Setting_Gap3['tile2'] }} TIỀN GỬI</b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>3 số cuối mã GD</td>
                                                                            <td><code>123</code> <code>234</code> <code>456</code> <code>678</code>
                                                                                <code>789</code> </td>
                                                                            <td><b>x{{ $Setting_Gap3['tile3'] }} TIỀN GỬI</b></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                - <b>Lưu ý : Mức cược mỗi số khác nhau, nếu chuyển sai hạn mức hoặc sai nội dung sẽ không
                                                    được hoàn tiền.</b>
                                        </div>


                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                    <!-- COL END -->
                    <div class="col-xl-3 col-md-12">
                        <div class="card ">
                            <div class="card-header">
                                <h5 class="card-title"><b>TRẠNG THÁI MOMO</b></h5>
                            </div>
                            <div class="card-body" id="momoStatus">
                                                 @foreach($ListAccounts as $row)
                                                 
                                                 <div class="media mb-5 mt-0">
                                                    <div class="d-flex me-3">
                                                        <img class="media-object rounded-circle thumb-sm" alt="64x64" src="https://e7.pngegg.com/pngimages/585/861/png-clipart-emoticon-avatar-kavaii-sina-weibo-chibi-cute-decorative-design-long-grass-yan-food-king.png">
                                                    </div>
                                                    <div class="media-body"> 
                                                    {{ $row->sdt }}
                                                    <span class="label label-{{ $row->status_class }} text-uppercase" onclick="coppy('{{ $row->sdt }}')"><i
                                                                class="fa fa-clipboard" aria-hidden="true"></i></span>
                                                        <div class="text-muted small">{{ $row->status_text }}</div>
                                                    </div> <button type="button" class="btn btn-{{ $row->status_class }} btn-sm d-block">{{ $row->status_text }}</button> 
                                                </div>
                                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- ROW-3 -->
                </div>


                <div class="row">
                    <div class="col-xl-4 col-md-12">
                        <div class="card overflow-hidden">
                            <div class="card-header">
                                <div>
                                    <h3 class="card-title"><b>TOP TUẦN</b></h3>
                                </div>
                            </div>
                            <div class="card-body pb-0 pt-4">
                            @php
                                        $dem = 0;
                                    @endphp
                                    @foreach($UserTopTuan as $row)
                                    @php
                                        $dem++;
                                    @endphp
                                    <div class="media mb-5">
                                        <div class="d-flex me-3">
                                            <img style="width: 66px;height: 66px;" class="media-object rounded-circle thumb-sm" src="https://e7.pngegg.com/pngimages/585/861/png-clipart-emoticon-avatar-kavaii-sina-weibo-chibi-cute-decorative-design-long-grass-yan-food-king.png">
                                        </div>
                                        <div class="media-body" style="font-size: 21px;">
                                        {{ number_format($row['tiencuoc']) }} xu
                                            <div class="text-muted small"> {{ $row['sdt2'] }}</div>
                                        </div>
                                        <button style="font-weight: 600;font-size: 25px;margin-top: 7px;border-radius: 30px;" type="button" class="btn btn-primary btn-sm d-block">{{ $dem }}</button>
                                    </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title fw-semibold "><b>THƯỞNG TUẦN</b></h4>
                            </div>
                            <div class="card-body pt-2 pb-2">
                            @if($GetSetting->on_trathuongtuan == 1)
                                    @php
                                        $dem = 0;
                                    @endphp
                                    
                                    @foreach($GetSettingPhanThuongTop as $row)
                                    @php
                                        $dem++;
                                    @endphp
                                    <div class="media mb-5">
                                        <div class="d-flex me-3">
                                            <img style="width: 66px;height: 66px;" class="media-object rounded-circle thumb-sm" src="https://e7.pngegg.com/pngimages/585/861/png-clipart-emoticon-avatar-kavaii-sina-weibo-chibi-cute-decorative-design-long-grass-yan-food-king.png">
                                        </div>
                                        <div class="media-body" style="font-size: 21px;">
                                        <b>TOP {{ $row->top }}</b>
                                            <div class="text-muted small"> {{ number_format($row->phanthuong) }} Xu</div>
                                        </div>
                                        <button style="font-weight: 600;font-size: 25px;margin-top: 7px;border-radius: 30px;" type="button" class="btn btn-primary btn-sm d-block">{{ $dem }}</button>
                                    </div>
                                    @if ($dem == 4)
                                        @break
                                    @endif
                                @endforeach
                            @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title fw-semibold "><b>HỖ TRỢ</b></h4>
                            </div>
                            <div class="card-body pb-0">
                                <ul class="task-list" style="padding: 49px;">
                                    <li> <i class="task-icon bg-primary"></i>
                                        <a href="{{ $GetSetting->contact_messeger }}">
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Facebook_Messenger_logo_2018.svg/768px-Facebook_Messenger_logo_2018.svg.png" style="width:50px;height:50px;margin-top: -11px;">
                                        <b style="color: #000;font-weight:600;margin-left: 30px;">MESSENGER</b> 
                                        </a>
                                    </li>
                                    <li> <i class="task-icon bg-secondary"></i>
                                    <a href="{{ $GetSetting->contact_zalo }}">
                                        <img src="https://i.imgur.com/SZAtCF7.png" style="width:50px;height:50px;margin-top: -11px;">
                                        <b style="color: #000;font-weight:600;margin-left: 30px;">ZALO</b>  
                                        </a>
                                    </li>
                                    <li> <i class="task-icon bg-primary"></i>
                                    <a href="{{ $GetSetting->contact_telegram }}">
                                    <img src="https://www.freepnglogos.com/uploads/telegram-logo-png-0.png" style="width:50px;height:50px;margin-top: -11px;">
                                        <b style="color: #000;font-weight:600;margin-left: 30px;">TELEGRAM</b>
                                        </a>
                                    </li>
                                    <li> <i class="task-icon bg-secondary"></i>
                                    <a href="{{ $GetSetting->contact_whatapps }}">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/YouTube_social_red_circle_%282017%29.svg/2048px-YouTube_social_red_circle_%282017%29.svg.png" style="width:50px;height:50px;margin-top: -11px;">
                                        <b style="color: #000;font-weight:600;margin-left: 30px;">YOUTUBE</b>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="card ">
                            <div class="card-header">
                                <h3 class="card-title mb-0"><b>LỊCH SỬ THẮNG</b></h3>
                            </div>
                            <div class="card-body">
                            <div class="table-responsive">
                                    <table class="table border text-nowrap text-md-nowrap table-bordered mg-b-0">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="background-color: #a6166f;color: #fff;font-weight: 600;">Thời gian</th>
                                                <th class="text-center" style="background-color: #a6166f;color: #fff;font-weight: 600;">Số điện thoại</th>
                                                <th class="text-center" style="background-color: #a6166f;color: #fff;font-weight: 600;">TIỀN GỬI</th>
                                                <th class="text-center" style="background-color: #a6166f;color: #fff;font-weight: 600;">Tiền nhận</th>
                                                <th class="text-center" style="background-color: #a6166f;color: #fff;font-weight: 600;">Trò chơi</th>
                                                <th class="text-center" style="background-color: #a6166f;color: #fff;font-weight: 600;">Nội dung</th>
                                                <th class="text-center" style="background-color: #a6166f;color: #fff;font-weight: 600;">Trạng thái</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($LichSuGiaoDich as $row)
                                            @php
                                                switch($row->noidung) {
                                                    case "C":
                                                            $spanClassColor = "#f44336";
                                                        break;
                                                    case "L":
                                                            $spanClassColor = "#9c27b0";
                                                        break;
                                                    case "C2":
                                                        $spanClassColor = "#4caf50";
                                                        break;
                                                        case "L2":
                                                        $spanClassColor = "#e74c3c";
                                                        break;
                                                        case "T":
                                                        $spanClassColor = "#3f51b5";
                                                        break;
                                                        case "X":
                                                        $spanClassColor = "#03a9f4";
                                                        break;
                                                        case "G3":
                                                        $spanClassColor = "#8a6d3b";
                                                        break;
                                                        default:
                                                        $spanClassColor = "#fff";
                                                        break;
                                                }
                                            @endphp


                                            <tr>
                                                <td class="text-center">{{ $row->created_at }}</td>
                                                <td class="text-center">{{ $row->sdt2 }}</td>
                                                <td class="text-center">{{ number_format($row->tiencuoc) }}</td>
                                                <td class="text-center">{{ number_format($row->tiennhan) }}</td>
                                                <td class="text-center">{{ $row->trochoi }}</td>
                                                <td class="text-center">
                                                <span class="fa-stack">
                                                        <span class="fa fa-circle fa-stack-2x" style="color:{{ $spanClassColor }};"></span>
                                                        <span class="fa-stack-1x text-white" style="color: #fff !important;">{{ strtoupper ($row->noidung) }} </span>
                                                </span>    
                                                <td class="text-center"> <span class="label label-{{ $row->class }} text-uppercase"> {{ $row->text }} </span></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- COL END -->
                </div>
@endsection
