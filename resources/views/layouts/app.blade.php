<!doctype html>
<html lang="en" dir="ltr">

<head>
    <!-- META DATA -->
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="fxmomo.com">
    <meta name="description" content="{{ $GetSetting->description }}" />
    <meta name="keywords" content="chan le momo, minigame momo, Chẵn Lẽ MoMo, Tài Xỉu momo, chanlemomo, Chẵn lẻ online, Chẵn Lẻ, momo cl, Cách Chơi chẵn lẽ momo,chẵn lẽ momo tự động , fxmomo" />
    <meta name="robots" content="index, follow">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:type" content="article">
    <meta property="og:title" content="{{ $GetSetting->description }}">
    <meta property="og:image" content="https://fxmomo.com/cover_real1.png" />
    <meta property="og:description" content="{{ $GetSetting->description }}">
    <!-- TITLE -->
    <title>{{ $GetSetting->title }}</title>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('image/favicon.png') }}" />
    <!-- BOOTSTRAP CSS -->
    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!-- STYLE CSS -->
    <link href="{{ asset('assets/css/stylesheet.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/dark-style.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/skin-modes.css') }}" rel="stylesheet" />
    <!-- SIDE-MENU CSS -->
    <link href="{{ asset('assets/css/sidemenu-icontext.css') }}" rel="stylesheet" id="sidemenu-theme">
    <!-- P-scroll bar css-->
    <link href="{{ asset('assets/plugins/p-scroll/perfect-scrollbar.css') }}" rel="stylesheet" />
    <!--- FONT-ICONS CSS -->
    <link href="{{ asset('assets/plugins/icons/icons.css') }}" rel="stylesheet" />
    <!-- SIDEBAR CSS -->
    <link href="{{ asset('assets/plugins/sidebar/sidebar.css') }}" rel="stylesheet" />

    <!-- SELECT2 CSS -->
    <link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" />
    <!-- INTERNAL Data table css -->
    <link href="{{ asset('assets/plugins/datatable/css/dataTables.bootstrap5.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/datatable/responsive.bootstrap5.css') }}" rel="stylesheet" />

    <!-- COLOR SKIN CSS -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{ asset('assets/colors/color1.css') }}" />
    <!-- INTERNAL Switcher css -->
    <link href="{{ asset('assets/switcher/css/switcher.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/switcher/demo.css') }}" rel="stylesheet" />
    <!-- FontAwesome 6.0.0 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" />
    <!-- JQUERY JS -->
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- NOTIFLIX -->
    <script src="{{ asset('assets/plugins/notiflix/dist/notiflix-aio-2.7.0.min.js') }}"></script>
    <!-- MOMENT TIMEZONE -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script src="https://momentjs.com/downloads/moment-timezone-with-data-1970-2030.min.js"></script>
    <script>
        moment().tz("Asia/Ho_Chi_Minh").format();
    </script>
    @yield('style')
</head>

<body class="app sidebar-mini">
    <div id="page">
        <div class="page-main">
            <!--APP-SIDEBAR-->
            <aside class="app-sidebar">
                <div class="side-header active">
                    <a class="header-brand1" href="/">
                        <img src="{{ $GetSetting->logo}}" class="header-brand-img desktop-logo" alt="logo">
                        <img src="/assets/images/brand/logo-1.png" class="header-brand-img toggle-logo" alt="logo">
                        <img src="/assets/images/brand/logo-2.png" class="header-brand-img light-logo" alt="logo">
                        <img src="{{ $GetSetting->logo}}" class="header-brand-img light-logo1" alt="logo">
                    </a>
                    <!-- LOGO -->
                </div>
                <ul class="side-menu">
                    <li>
                        <h3>MENU</h3>
                    </li>
                    <li class="slide active">
                        <a class="side-menu__item active" data-bs-toggle="slide" href="/">
                            <i class="side-menu__icon fe fe-home"></i><span class="side-menu__label">Trang Chủ</span>
                        </a>
                    </li>
                    <li>
                        <a class="side-menu__item" href="{{ $GetSetting->url1 }}"><i class="side-menu__icon fe fe-info"></i>
							<span class="side-menu__label">Giới thiệu</span> </a>
                    </li>
                    <li>
                        <a class="side-menu__item" href="{{ $GetSetting->url2 }}"><i class="side-menu__icon fe fe-globe"></i>
							<span class="side-menu__label">Hướng dẫn</span> </a>
                    </li>
                    <li>
                        <a class="side-menu__item" href="{{ $GetSetting->url3 }}"><i class="side-menu__icon fe fe-edit"></i>
                            <span class="side-menu__label">Blog</span>
						</a>
                    </li>
                    <li>
                        <a class="side-menu__item" href="{{ $GetSetting->url4 }}"><i class="side-menu__icon fe fe-message-circle"></i>
							<span class="side-menu__label">Live Chat Private</span> </a>
                    </li>
                    <li class="justify-content-center" style="margin-top:20px;">
                        <center>
                            <span class="avatar avatar-md brround mt-1" style="width: 74px;height: 74px;background-image: url(https://fxmomo.com/phuong-uyen.jpg);"></span> <br>
                            <br>
                            <b style="color:#5a5a5a;font-weight: 600;">Lê Phương Uyên</b>
                        </center>
                    </li>

                </ul>
            </aside>
            <!--/APP-SIDEBAR-->

            <!-- Mobile Header -->
            <div class="app-header header">
                <div class="container-fluid">
                    <div class="d-flex">
                        <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-bs-toggle="sidebar" href="#"></a>
                        <!-- sidebar-toggle-->
                        <a class="header-brand1 d-flex d-md-none" href="/">
                            <img src="{{ $GetSetting->logo}}" class="header-brand-img desktop-logo" alt="logo">
                            <img src="/assets/images/brand/logo-1.png" class="header-brand-img toggle-logo" alt="logo">
                            <img src="/assets/images/brand/logo-2.png" class="header-brand-img light-logo" alt="logo">
                            <img src="{{ $GetSetting->logo}}" class="header-brand-img light-logo1" alt="logo">
                        </a>
                        <!-- LOGO -->
                        <div class="main-header-center ms-3 d-none d-md-block">
                            <input id="transIdInput" type="number" class="form-control" placeholder="Kiểm tra mã giao dịch" type="search">
                            <button class="btn" id="btnCheckTransID" onclick="TransIDCheck($('#transIdInput').val())"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                        <!-- Theme-Layout -->
                        <div class="d-flex order-lg-2 ms-auto header-right-icons">
                            <div class="dropdown d-lg-none d-md-block d-none"> <a href="#" class="nav-link icon" data-bs-toggle="dropdown"> <i
                                        class="fe fe-search"></i> </a>
                                <div class="dropdown-menu header-search dropdown-menu-start">
                                    <div class="input-group w-100 p-2"> <input type="text" class="form-control" placeholder="Search....">
                                        <div class="input-group-text btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- SEARCH --> 
                            <button class="navbar-toggler navresponsive-toggler d-md-none ms-auto active" type="button"
                                data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4"
                                aria-expanded="true" aria-label="Toggle navigation"> <span
                                    class="navbar-toggler-icon fe fe-search text-dark"></span> </button>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="mb-1 navbar navbar-expand-lg  responsive-navbar navbar-dark d-md-none bg-white">
            <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
                <div class="d-flex order-lg-2 ms-auto">
                    <div class="input-group w-70 p-2 ">
                        <input  type="number" id="transIDMobile" class="form-control" placeholder="Tìm kiếm mã giao dịch....">
                        <div class="input-group-text btn btn-primary" onclick="TransIDCheck($('#transIDMobile').val())">
                            <i class="fa fa-search" aria-hidden="true"></i> 
                        </div>
                    </div>
                </div>
            </div>
    </div>
        <!-- /Mobile Header -->

    </div>

    <!--app-content open-->

    <div class="app-content">
        <div class="side-app">
                @yield('script')
                @yield('checkTransID')
                @yield('thongbao')
                @yield('content')
        </div>
    </div>


    <!-- end app-content open-->


    <!-- Footer -->
    <footer class="footer">
        <div class="container"> <div class="row align-items-center flex-row-reverse">

        <div class="card-body">
    <div class="top-footer">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <h3>Giới thiệu</h3>
                <p style="text-align: justify"><b><a href="https://fxmomo.com" style="color: #952a7a">Chẵn lẻ momo</a></b> là một loại trò chơi giải trí, có thể giúp bạn kiếm tiền nhanh chóng chỉ sau vài thao tác trên momo. nó thuộc loại trò chơi cá cược và hiện đang khá là phổ biến trong giới trẻ hiện nay bởi tính minh bạch, xanh chín của nó</p>
                <p style="text-align: justify"><b><a href="https://blog.fxmomo.com" style="color: #952a7a">MiniGame Chẵn lẻ momo</a></b> là trò chơi cá cược chuyển tiền dựa trên mã giao dịch kết thúc trong Ví Momo. mà các bạn sẽ có những lựa chọn chẵn hoặc lẻ, tài hoặc xỉu để điền vào nội dung lúc chuyển khoản. Nếu đoán đúng, các bạn sẽ nhận được tiền từ hệ thống tự động chuyển khoản lại ngay sau đó 10s - 30s</p>
            </div>
            <div class="col-lg-2 col-md-12">
                
            </div>
            <div class="col-lg-2 col-md-12">
            </div>
            <div class="col-lg-4 col-md-12">
                <h3>Thông tin</h3>
                <ul class="list-unstyled mb-4">
                    <li><a href="#">Chính sách bảo mật</a></li>
                    <li><a href="#">Điều khoản sử dụng</a></li>
                    <li><a href="#">Tuyên bố & Trách nhiệm</a></li>
                </ul>
                <h3>Liên hệ</h3>
                <ul class="list-unstyled mb-4">
                    <li><a href="#">Call/Zalo/Viber: +84763368773</a></li>
                    <li><a href="#">Mail: info@fxmomo.com</a></li>
                </ul>
            
            </div>
            
        </div>
    </div>
    <div class="row align-items-center">
    <div class="social">
        <ul class="text-center">
            <li> <a class="social-icon" href=""><i class="fa-brands fa-facebook"></i></a> </li>
            <li> <a class="social-icon" href=""><i class="fa-brands fa-twitter"></i></a> </li>
            <li> <a class="social-icon" href=""><i class="fa-brands fa-youtube"></i></a> </li>
            <li> <a class="social-icon" href=""><i class="fa-brands fa-linkedin"></i></a> </li>
            <li> <a class="social-icon" href=""><i class="fa-brands fa-google-plus"></i></a> </li>
        </ul>
    </div>
    <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center"> Copyright © 2021 <a href="#">FXMOMO</a>. Designed by <a href="#">Spruko</a> All rights reserved. </div>
   <center><a href="//www.dmca.com/Protection/Status.aspx?ID=c073b657-735e-4f6e-9471-b12083dc7ee6" title="DMCA.com Protection Status" class="dmca-badge"> <img src ="https://images.dmca.com/Badges/dmca-badge-w150-5x1-09.png?ID=c073b657-735e-4f6e-9471-b12083dc7ee6"  alt="DMCA.com Protection Status" /></a>  <script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script></center>
</div>
</div>
         </div>
      </div>
    </footer>
    <!--/Footer-->
    <!-- BOOTSTRAP JS -->
    <script src="{{ asset('assets/plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- INPUT MASK JS-->
    <script src="{{ asset('assets/plugins/input-mask/jquery.mask.min.js') }}"></script>

    <!-- SIDEBAR JS -->
    <script src="{{ asset('assets/plugins/sidebar/sidebar.js') }}"></script>

    <!-- SIDE-MENU JS-->
    <script src="{{ asset('assets/plugins/sidemenu/sidemenu2.js') }}"></script>

    <!-- CHARTJS CHART JS-->
    <script src="{{ asset('assets/plugins/chart/Chart.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/chart/utils.js') }}"></script>

    <!-- PIETY CHART JS-->
    <script src="{{ asset('assets/plugins/peitychart/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/peitychart/peitychart.init.js') }}"></script>

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{ asset('assets/plugins/select2/select2.full.min.js') }}"></script>

    <!-- INTERNAL Data tables js-->
    <script src="{{ asset('assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatable/js/dataTables.bootstrap5.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>

    <!-- ECHART JS-->
    <script src="{{ asset('assets/plugins/echarts/echarts.js') }}"></script>

    <!-- APEXCHART JS -->
    <script src="{{ asset('assets/js/apexcharts.js') }}"></script>

    <!-- CUSTOM JS-->
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <!-- Switcher js -->
    <script src="{{ asset('assets/switcher/js/switcher.js') }}"></script>
</body>

</html>